// Code goes here
var app = angular.module('org.zcode.lexec.App',['ngRoute', 'org.zcode.lexec.Solutions','org.zcode.lexec.BpelUtils', 'org.zcode.lexec.Admin','org.zcode.lexec.Reports']);

app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
      .when('/', {
   		templateUrl : "../pages/modules/common/dashboard.html"
      }).when("/dashboard", {
   		templateUrl : "../pages/modules/common/dashboard.html"
      }).when("/login_page", {
   		templateUrl : "../pages/login_page.html"
      }).when("/error_page", {
     		templateUrl : "../pages/error_page.html"
      }).when("/data_analysis", {
  		templateUrl : "../pages/modules/solutions/data_analysis.html"
      }).when("/search_solution", {
		templateUrl : "../pages/modules/solutions/search_solution.html"
      }).when("/list_all_solutions", {
  		templateUrl : "../pages/modules/solutions/list_all_solutions.html"
      }).when("/usage_statistics", {
    		templateUrl : "../pages/modules/common/usage_statistics.html"
	  }).when("/bpel_utils", {
			templateUrl : "../pages/modules/bpelutils/bpel_utils.html"
      }).when("/reports/search", {
  		templateUrl : "../pages/modules/solutions/new_solution.html"
      }).when("/reports/showReport", {
  		templateUrl : "../pages/modules/reports/report_view.html"
      }).when("/admin/new_solution", {
		templateUrl : "../pages/modules/solutions/new_solution.html"
	  }).when("/admin/task_logs", {
			templateUrl : "../pages/modules/common/task_logs.html"
	  }).when("/admin/new_datasource", {
			templateUrl : "../pages/modules/admin/new_datasource.html"
	  }).when("/admin/new_report", {
			templateUrl : "../pages/modules/admin/new_report.html"
	  }).when("/superadmin/new_user", {
			templateUrl : "../pages/modules/superadmin/new_user.html"
	});
	  
}]);

app.controller('UsageStatisticsController', function ($scope, $http){ // , $mdDialog) {
/*	$http.get('../rest/fetchUsageStatistics').
    then(function (response) {
        $scope.usageStatistics = response.data;
    }, function (response) {
    	$scope.usageStatistics
    }); */
});

app.controller('HomePageController', function ($scope, $http){ // , $mdDialog) {
	
	$scope.request = new Object();
	
	$http.get('../rest/getUserEnvs').
	    then(function (response) {
	        $scope.bpelEnvs = response.data['bpelEnvs'];//$sce.trustAsHtml(response.data);
	        $scope.teleEnv = response.data['teleEnv'];
	        $scope.request.env = $scope.teleEnv;
	    }, function (response) {
	        $scope.bpelEnvs = "";
	    });
	
	$http.get('../rest/getAllGroups').
	    then(function (response) {
	        $scope.groups = response.data;//$sce.trustAsHtml(response.data);
	    }, function (response) {
	        $scope.groups = '';
	    });
	
	$http.get('../rest/reports/fetchAll').
	    then(function (response) {
	        $scope.reports = response.data;//$sce.trustAsHtml(response.data);
	    }, function (response) {
	        $scope.reports = '';
	    });
	
	$http.get('../rest/isAdminRole').
	    then(function (response) {
	        $scope.isAdmin = response.data;//$sce.trustAsHtml(response.data);
	    }, function (response) {
	        $scope.isAdmin = "false";
	    });
	
	$http.get('../rest/isSuperAdminRole').
	    then(function (response) {
	        $scope.isSuperAdmin = response.data;//$sce.trustAsHtml(response.data);
	    }, function (response) {
	        $scope.isSuperAdmin = "false";
	    });
	/*
	var authenticate = function(credentials, callback) {

	    var headers = credentials ? {authorization : "Basic "
	        + btoa(credentials.username + ":" + credentials.password)
	    } : {};

	    $http.get('user', {headers : headers}).success(function(data) {
	      if (data.name) {
	        $rootScope.authenticated = true;
	      } else {
	        $rootScope.authenticated = false;
	      }
	      callback && callback();
	    }).error(function() {
	      $rootScope.authenticated = false;
	      callback && callback();
	    });

	  }

	 authenticate();
	 $scope.credentials = {};
	 $scope.login = function() {
	      authenticate($scope.credentials, function() {
	        if ($rootScope.authenticated) {
	          $location.path("/");
	          $scope.error = false;
	        } else {
	          $location.path("/login");
	          $scope.error = true;
	        }
	      });
	  };
	*/
});
