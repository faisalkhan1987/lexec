// Code goes here
var solutionsModule = angular.module('org.zcode.lexec.Admin', []); 
solutionsModule.controller('AdminController', function ($scope, $http, $window){ // , $mdDialog) {


	$scope.ds = new Object();
	
	$scope.saveDS = function(){
		$http.post("../rest/admin/post", $scope.ds).
            then(function (response) {
            	$window.alert('DS has been saved succesfully');
				$scope.ds = '';
            }, function (response) {
            	$window.alert('Unexpected failure occurred');
				console.log(response);
            });
	}
	
});

solutionsModule.controller('SuperAdminController', function ($scope, $http, $window){ // , $mdDialog) {

	$scope.allroles = ['USER', 'ADMIN', 'BPEL_USER'];
	$scope.allBpelEnvs = ['PRD', 'UAT', 'TRG', 'INT', 'SUP', 'LAT', 'EDP', 'DVP'];
	
	$scope.user = new Object();
	$scope.user.roles = [];
	$scope.user.bpelEnvs = [];
	
	$scope.saveUser = function(){
		$http.post("../rest/admin/saveUser", $scope.user).
            then(function (response) {
            	$window.alert('User created succesfully');
				$scope.ds = '';
            }, function (response) {
            	$window.alert('Unexpected failure occurred');
				console.log(response);
            });
	}
	
	$scope.toggleSelection = function toggleSelection(role) {
	    var idx = $scope.user.roles.indexOf(role);

	    // is currently selected
	    if (idx > -1) {
	    	$scope.user.roles.splice(idx, 1);
	    }

	    // is newly selected
	    else {
	    	$scope.user.roles.push(role);
	    }
	  };
	  
	  $scope.toggleBpelEnvSelection = function toggleBpelEnvSelection(bpelEnv) {
		    var idx = $scope.user.bpelEnvs.indexOf(bpelEnv);

		    // is currently selected
		    if (idx > -1) {
		    	$scope.user.bpelEnvs.splice(idx, 1);
		    }

		    // is newly selected
		    else {
		    	$scope.user.bpelEnvs.push(bpelEnv);
		    }
		};
	
});

