// Code goes here
var solutionsModule = angular.module('org.zcode.lexec.Reports', []); 
solutionsModule.controller('ReportsController', function ($scope, $http, $routeParams){ // , $mdDialog) {


	$scope.report = new Object();
	$scope.response = new Object();
	
	$scope.saveReport = function(){
		$http.post("../rest/reports/post", $scope.report).
            then(function (res) {
            	$scope.response.type='success';
            	$scope.response.message = 'Report saved successfully';
				$scope.report = '';
            }, function (res) {
            	$scope.response.type='failure';
            	$scope.response.message = 'Error occured';
				console.log(response);
            });
	}
	
	if($routeParams.reportId){
		$scope.workInProgress = true;
		$http.get('../rest/reports/fetch?reportId='+$routeParams.reportId).
			then(function (response) {
		        $scope.report = response.data;
				$scope.data = {
						  "cols": $scope.report.cols,
						  "rows": $scope.report.rows 
						};
				
				google.charts.load('current', {'packages':['corechart']});
				google.charts.setOnLoadCallback(drawChart);
		        $scope.workInProgress = false;
		    }, function (response) {
		        console.log('failure');
				console.log(response);
				$scope.workInProgress = false;
		    });


      function drawChart() {
        var data = new google.visualization.DataTable($scope.data);
        var options = {
          title: 'BPEL Errors',
          legend: { position: 'bottom' },
		   tooltip: {isHtml: true},
        };
	        
        var chart = new google.visualization.ColumnChart(document.getElementById('bar_chart'));

        chart.draw(data, options);
      }
	}
	

});
