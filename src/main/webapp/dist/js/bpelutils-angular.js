// Code goes here
var bpelUtilsApp = angular.module('com.selfcare.BpelUtils', []);

bpelUtilsApp.controller('BpelUtilsController', function ($scope, $http, $timeout) {

	$scope.workInProgress = false;
	$scope.detailedTrace = false;
	
	$scope.modifiedVariables = [];
	
	$scope.selectValue = function(key, value){
		if($scope.selectedVariable != null && $scope.selectedVariable != ''){

			var modifiedValue = document.getElementById('selectedValue').value;
			if($scope.bpelResponse.bpelVariables[$scope.selectedVariable] != modifiedValue){

				$scope.modifiedVariables.push($scope.selectedVariable);
				$scope.bpelResponse.bpelVariables[$scope.selectedVariable] = modifiedValue;
			}
		}
		$scope.selectedVariable = key;
		$scope.selectedValue = value;
		document.getElementById('selectedValue').value = value;
	}

	$scope.showDetailedTrace = function(){
		$scope.detailedTrace = true;
	}
	$scope.showLess = function(){
		$scope.detailedTrace = false;
	}
	
	$scope.emptyVariables = function(){
		$scope.bpelResponse = '';
		$scope.selectedVariable = '';
		$scope.selectedValue = '';
	}
	
	$scope.fetchDetails = function(){
//		console.log($scope.request);
		$scope.emptyVariables();
		$scope.workInProgress = true;
		$http.post('../rest/bpelutils/fetchDetails', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				$scope.workInProgress = false;
			}, function (response) {
				console.log('Failed');
				$scope.workInProgress = false;
			});
	};

	$scope.recoverFault = function(){
		$scope.emptyVariables();
		if(!$scope.request.faultId.startsWith('wls') && $scope.request.faultId.length > 500){
			alert('You are not authorized to do this operation');
			return;
		}
		$scope.workInProgress = true;		
		
		$http.post('../rest/bpelutils/recoverFault', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				alert('Recovered succesfully');
				$scope.workInProgress = false;
			}, function (response) {
				alert('Recovery failed');
				$scope.workInProgress = false;
			});
	}

	$scope.resubmitFault = function(){
		$scope.emptyVariables();
		if(!$scope.request.faultId.startsWith('wls') && $scope.request.faultId.length > 500){
			alert('You are not authorized to do this operation');
			return;
		}
		$scope.workInProgress = true;
		
		$http.post('../rest/bpelutils/resubmitFault', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				alert('Resubmitted succesfully');
				$scope.workInProgress = false;
			}, function (response) {
				alert('Resubmit failed');
				$scope.workInProgress = false;
			});
	}

	$scope.continueFault = function(){
		$scope.emptyVariables();
		if(!$scope.request.faultId.startsWith('wls') && $scope.request.faultId.length > 500){
			alert('You are not authorized to do this operation');
			return;
		}
		$scope.workInProgress = true;
		
		$http.post('../rest/bpelutils/continueFault', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				alert('Continued succesfully');
				$scope.workInProgress = false;
			}, function (response) {
				alert('Continue failed');
				$scope.workInProgress = false;
			});
	}

	$scope.abortInstance = function(){
		$scope.emptyVariables();
		if(!$scope.request.faultId.startsWith('wls') && $scope.request.faultId.length > 500){
			alert('You are not authorized to do this operation');
			return;
		}
		$scope.workInProgress = true;
		
		$http.post('../rest/bpelutils/abortInstance', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				alert('Aborted succesfully');
				$scope.workInProgress = false;
			}, function (response) {
				alert('Abort failed');
				$scope.workInProgress = false;
			});
	}

	$scope.retryFault = function(value){
		if(!$scope.request.faultId.startsWith('wls') && $scope.request.faultId.length > 500){
			alert('You are not authorized to do this operation');
			return;
		}
		$scope.workInProgress = true;

		$scope.selectedValue = value;
		
		if($scope.selectedVariable != null && $scope.selectedVariable != ''){
			console.log('selected variable not null');
			var modifiedValue = document.getElementById('selectedValue').value;
			if($scope.bpelResponse.bpelVariables[$scope.selectedVariable] != modifiedValue){
				console.log('modified');
				$scope.modifiedVariables.push($scope.selectedVariable);
				$scope.bpelResponse.bpelVariables[$scope.selectedVariable] = modifiedValue;
				console.log($scope.modifiedVariables);
				console.log(modifiedValue);
			}
		}
		
//		$scope.workInProgress = false;
		var modifiedBpelVariables = [];
		
		$scope.modifiedVariables.forEach(function(entry){
			console.log(entry);
			var bpelVariable = new Object();
			bpelVariable.name = entry;
			bpelVariable.value = $scope.bpelResponse.bpelVariables[entry];
			modifiedBpelVariables.push(bpelVariable);
		});
		
		$scope.request.modifiedVariables = modifiedBpelVariables;
		console.log($scope.request.modifiedVariables);
		$scope.emptyVariables();
		$http.post('../rest/bpelutils/retryFault', $scope.request).
			then(function (response) {
				$scope.bpelResponse = response.data;
				alert('Retry succesfull');
				$scope.workInProgress = false;
			}, function (response) {
				alert('Retry failed');
				$scope.workInProgress = false;
			}); 
	}


	
});
