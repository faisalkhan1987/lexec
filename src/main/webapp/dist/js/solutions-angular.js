// Code goes here
var solutionsModule = angular.module('org.zcode.lexec.Solutions', []); /* ['ngMaterial']); */

solutionsModule.controller('SolutionsController', function ($scope, $http, $interval){ // , $mdDialog) {

	$scope.inprogress = false;
	
	$scope.startAnalysis = function(){
		$http.get("../rest/strategy/triggerDataAnalysis?inputType="+ $scope.selfcareForm.inputType + "&inputValue="+ $scope.selfcareForm.inputValue).
            then(function (response) {
                $scope.threadId = response.data.threadId;
                $scope.fetchLog();
            }, function (response) {
                console.log('failure');
				console.log(response);
            }); 
	}
	
	$scope.fetchLogs = function(){
		$http.get('../rest/strategy/fetchLogsByThreadId?threadId='+$scope.selfcareForm.threadId).
    	  		then(function (response) {
    	              $scope.taskLogs = response.data;
    	          }, function (response) {
    	              console.log('failure');
    	  			  console.log(response);
    	          });
	}
	
	var logger;
    $scope.fetchLog = function() {
      $scope.inprogress = true;
      var startTime = new Date().getTime();
      $scope.taskLogs = [];
      // Don't start a new logger if a logger is already defined
      if ( angular.isDefined($scope.threadId) ) {
    	  logger = $interval(function() {
    	  	  if(new Date().getTime() - startTime > 60000){
    	  	  	console.log('Stop Logger Due to Max time');
    	  	  	$scope.stopLogger();
    	  	  }
        	  $http.get('../rest/strategy/fetchLogsByThreadId?threadId='+$scope.threadId).
    	  		then(function (response) {
    	              $scope.taskLogs = response.data;
    	          }, function (response) {
    	              console.log('failure');
    	  			  console.log(response);
    	  			  $scope.inprogress = false;
    	          });
          }, 500);
    	  
      }
      
    };
    
    $scope.stopLogger = function(){
    	$interval.cancel(logger);
    	$scope.inprogress = false;
    }
});

solutionsModule.controller('NewSolutionController', function ($scope, $http, $window){ // , $mdDialog) {

	$scope.strategy = new Object();
	$scope.strategy.inputParams = [];
	$scope.strategy.passwordParams = [];
	$scope.strategy.tasks = [];	
	$scope.dbs = [];
	
	$http.get('../rest/getAllDbs').
        then(function (response) {
            $scope.dbs = response.data;//$sce.trustAsHtml(response.data);
        }, function (response) {
            $scope.dbs = "";
    });
    
	$scope.saveInputParam = function(){
		if($scope.selfcareForm.paramType == 'password'){
			$scope.strategy.passwordParams.push($scope.selfcareForm.inputParam);
		}else{
			$scope.strategy.inputParams.push($scope.selfcareForm.inputParam);
		}
		$scope.selfcareForm.inputParam = '';
	};
	
	$scope.removeParam = function(index){
		$scope.strategy.inputParams.splice(index, 1);
	}

	$scope.removePasswordParam = function(index){
		$scope.strategy.passwordParams.splice(index, 1);
	}

	$scope.removeTask = function(index){
		$scope.strategy.tasks.splice(index, 1);
	}
	
	$scope.saveSQL = function(){
		$scope.selfcareForm.sqlVO.type = "SQL";
		$scope.strategy.tasks.push($scope.selfcareForm.sqlVO);
		$scope.selfcareForm.sqlVO = '';
	}
	
	$scope.saveSoapWS = function(){
		$scope.selfcareForm.soapVO.type = "SOAP";
		$scope.strategy.tasks.push($scope.selfcareForm.soapVO);
		$scope.selfcareForm.soapVO = '';
	}
	
	$scope.saveIfControl = function(){
		$scope.selfcareForm.controlVO = new Object();
		$scope.selfcareForm.controlVO.type = "CONTROL";
		$scope.selfcareForm.controlVO.expression = "if " + $scope.selfcareForm.ifControlVO.expression;
		$scope.strategy.tasks.push($scope.selfcareForm.controlVO);
	}

	$scope.saveEndIfControl = function(){
		$scope.selfcareForm.controlVO = new Object();
		$scope.selfcareForm.controlVO.type = "CONTROL";
		$scope.selfcareForm.controlVO.expression = "endif";
		$scope.strategy.tasks.push($scope.selfcareForm.controlVO);
	}
	
	$scope.saveAlertControl = function(){
		$scope.selfcareForm.controlVO = new Object();
		$scope.selfcareForm.controlVO.type = "CONTROL";
		$scope.selfcareForm.controlVO.expression = "alert #"+$scope.selfcareForm.alertControlVO.expression;
		$scope.strategy.tasks.push($scope.selfcareForm.controlVO);
	}

	$scope.saveBpelControl = function(action){
		$scope.selfcareForm.controlVO = new Object();
		$scope.selfcareForm.controlVO.type = "CONTROL";
		if(action == 'RESUBMIT'){
			$scope.selfcareForm.controlVO.expression = "bpelresubmit "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'CONTINUE'){
			$scope.selfcareForm.controlVO.expression = "bpelcontinue "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'RECOVER'){
			$scope.selfcareForm.controlVO.expression = "bpelrecover "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'ABORT'){
			$scope.selfcareForm.controlVO.expression = "bpelabort "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'RETRY'){
			$scope.selfcareForm.controlVO.expression = "bpelretry "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'FETCH_FAULT_DETAILS'){
			$scope.selfcareForm.controlVO.expression = "bpelfetchfault "+$scope.selfcareForm.bpelControlVO.expression;
		}else if(action == 'REPLACE_VARIABLE'){
			$scope.selfcareForm.controlVO.expression = "bpelreplacevariable "+$scope.selfcareForm.bpelControlVO.expression;
			$scope.selfcareForm.controlVO.inputParams = $scope.selfcareForm.bpelControlVO.inputParams;
		}else if(action == 'REPLACE_VARIABLE_INSERT'){
			$scope.selfcareForm.controlVO.expression = "bpelinsertvariable "+$scope.selfcareForm.bpelControlVO.expression;
			$scope.selfcareForm.controlVO.inputParams = $scope.selfcareForm.bpelControlVO.inputParams;
		}
		$scope.strategy.tasks.push($scope.selfcareForm.controlVO);
	}
	
	$scope.saveExecuteControl = function(){
		$scope.selfcareForm.controlVO.type = "CONTROL";
		$scope.selfcareForm.controlVO.expression = "execute #"+$scope.selfcareForm.controlVO.solutionId;
		$scope.strategy.tasks.push($scope.selfcareForm.controlVO);
	}
	
	$scope.saveStrategy = function(){
		$http.post("../rest/strategy/post", $scope.strategy).
            then(function (response) {
            	$window.alert('Strategy has been saved succesfully');
				$scope.strategy = '';
            }, function (response) {
                console.log('failure');
				console.log(response);
            });
	}
	
});

solutionsModule.controller('SolutionSearchController', function ($scope, $http){ // , $mdDialog) {

	$scope.ctasks = [];
	$scope.searchSolution = function(){
		$http.get("../rest/strategy/fetchBySearchTxt?searchTxt="+ $scope.selfcareForm.searchTerm).
            then(function (response) {
                $scope.ctasks = response.data;
            }, function (response) {
                console.log('failure');
				console.log(response);
            }); 
	}
});

solutionsModule.controller('SolutionListController', function ($scope, $http, $routeParams){ // , $mdDialog) {

	$scope.ctasks = [];
	$http.get("../rest/strategy/fetchByGroup?groupName="+$routeParams.groupName).
           then(function (response) {
               $scope.ctasks = response.data;
           }, function (response) {
               console.log('failure');
			console.log(response);
           }); 
});

solutionsModule.controller('SolutionDetailsController', function ($scope, $http, $interval, $routeParams, $window){ // , $mdDialog) {
	$scope.strategy = new Object();
	$scope.strategy.inputParams = [];
	$scope.strategy.passwordParams = [];
	$scope.taskLogs = [];
	$scope.threadId=null;
	
	$http.get('../rest/isSuperAdminRole').
	    then(function (response) {
	        $scope.isSuperAdmin = response.data;//$sce.trustAsHtml(response.data);
	    }, function (response) {
	        $scope.isSuperAdmin = "false";
	    });
	
	$http.get('../rest/strategy/getById?taskId='+$routeParams.solutionId).
		then(function (response) {
            $scope.ctask = response.data;
        }, function (response) {
            console.log('failure');
			console.log(response);
        });

	$scope.approveTask = function(ctask){
		$scope.strategy.taskId = ctask.taskId;
		$http.post("../rest/strategy/approve", $scope.strategy).
            then(function (response) {
            	if(response.data.status == 'FAILURE'){
            		$window.alert('Failure '+ response.data.errorMessage);
            	}else{
            		$window.alert('The workaround is approved');
            		$scope.ctask.approved = true;
            	}
            }, function (response) {
                console.log('failure');
				console.log(response);
            });
	}

	$scope.executeTask = function(ctask){
		$scope.strategy.taskId = ctask.taskId;
		$http.post("../rest/strategy/execute", $scope.strategy).
            then(function (response) {
            	if(response.data.status == 'FAILURE'){
            		$window.alert('Failure '+ response.data.errorMessage);
            	}else{
    				$scope.threadId = response.data.threadId;
    				$scope.fetchLog();
            	}
            }, function (response) {
                console.log('failure');
				console.log(response);
            });
	}
	
	$scope.executeBulkTask = function(ctask){
		$scope.strategy.taskId = ctask.taskId;
		$http.post("../rest/strategy/executeBulk", $scope.strategy).
            then(function (response) {
            	if(response.data.status == 'FAILURE'){
            		$window.alert('Failure '+ response.data.errorMessage);
            	}else{
					$scope.threadId = response.data.threadId;
					$scope.fetchLog();
            	}
            }, function (response) {
                console.log('failure');
				console.log(response);
            });
	}
	
	$scope.clear = function(){
		$scope.strategy = new Object();
		$scope.strategy.inputParams = [];
		$scope.strategy.passwordParams = [];
	}
	
	var logger;
    $scope.fetchLog = function() {
    	$scope.inprogress = true;
    	// Don't start a new logger if a logger is already defined
      if ( angular.isDefined($scope.threadId) ) {
    	  logger = $interval(function() {
        	  $http.get('../rest/strategy/fetchLogsByThreadId?threadId='+$scope.threadId).
    	  		then(function (response) {
    	              $scope.taskLogs = response.data;
    	          }, function (response) {
    	              console.log('failure');
    	  			  console.log(response);
    	  			$scope.inprogress = false;
    	  		});
          }, 200);
    	  
      }
      
    };
    
    $scope.stopLogger = function(){
		$scope.inprogress = false;
    	$interval.cancel(logger);
    }
});
