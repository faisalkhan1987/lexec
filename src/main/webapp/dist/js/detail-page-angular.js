// Code goes here
var app = angular.module('org.zcode.lexec.App',['ngRoute','org.zcode.lexec.Solutions']);//, ['com.tools.BpelUtils']);

app.config(function($routeProvider) {
  $routeProvider
  .when("/solution_detail_bulk", {
	templateUrl: '../pages/modules/solutions/solution_detail_bulk.html'
  })
  .when("/solution_detail", {
	templateUrl: '../pages/modules/solutions/solution_detail.html'
  });
});
