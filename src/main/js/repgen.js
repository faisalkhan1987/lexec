/**
 * 
 */
console.log('*** Generating Usage Reports ****')

var MongoClient = require('mongodb').MongoClient;
var serverip = 'localhost';
//var serverip = 'prod402.prod.telenet.be';
// Connect to the db
MongoClient.connect("mongodb://"+serverip+":27017/test", function(err, db) {
  if(!err) {
    console.log("We are connected");
  }
  
  var collection = db.collection('taskLog');
  var d = new Date();
  //d.setDate(d.getDate()-30);

  
  fetchData(/# Starting Execution of Task/, d);
  
  fetchData(/BpelRequest/, d);
  
  fetchData(/DataFetch#101/, d);
  
  
  db.close();
  
  function fetchData(strToSearch, dtParam){
		var cursor = collection.aggregate([
		                        		   { $match:{ log : strToSearch , createdDate : { $lte : dtParam } } },
		                        		   { $group :   {
		                        			   _id:   {  year : { $year : "$createdDate"  }, month : { $month : "$createdDate" }/*, day : { $dayOfMonth : "$createdDate" }*/ },
		                        			   count: { $sum: 1 } }
		                        		   },
		                        		   { $sort : { _id :  1 } }
		                        		   ]);
		cursor.each(function(err, doc) {
			   if(err)
				   throw err;
			   if(doc==null)
				   return;
			                        	 
			   console.log(strToSearch + ':' + JSON.stringify(doc));
			   //console.log(doc.count);
			});
		
	}
});

