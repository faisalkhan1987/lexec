package org.zcode.lexec.config;

import java.util.concurrent.Executor;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.zcode.lexec.constants.CommonConstants;
import org.zcode.lexec.domain.User;
import org.zcode.lexec.repo.UserRepo;
import org.zcode.lexec.rest.AdminController;
import org.zcode.lexec.rest.BpelUtilsController;
import org.zcode.lexec.rest.CommonDataController;
import org.zcode.lexec.rest.ReportsController;
import org.zcode.lexec.rest.StrategyController;

/**
 * @author faisal_khan01
 * org.zcode.lexec
 */
@Configuration
@EnableAsync
@ApplicationPath("/rest")
public class AppConfig extends ResourceConfig implements AsyncConfigurer{

    /**
     * Constructor that injects all restful services
     */
    public AppConfig() {
    	register(StrategyController.class);
    	register(BpelUtilsController.class);
    	register(AdminController.class);
    	register(CommonDataController.class);
    	register(ReportsController.class);
    }

    /**
     * WA/Strategy execution is asynchronous, the below settings helps us to manage thread pool 
     */
    @Override
    public Executor getAsyncExecutor() {
    	ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(7);
        executor.setMaxPoolSize(42);
        executor.setQueueCapacity(11);
        //executor.setThreadNamePrefix("MyExecutor-");
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
	// TODO Auto-generated method stub
	return null;
    }
    
}

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

  @Autowired
  UserRepo userRepo;

  @Override
  public void init(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService());
  }

  @Bean
  UserDetailsService userDetailsService() {
    return new UserDetailsService() {

      @Override
      public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if(user != null) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true,
                AuthorityUtils.createAuthorityList(user.getRoles()));
        } else {
          throw new UsernameNotFoundException("could not find the user '"
                  + username + "'");
        }
      }
      
    };
  }
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
 
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
    	.authorizeRequests()
    		.anyRequest().fullyAuthenticated()
    		.antMatchers("pages/**/admin/**").hasRole(CommonConstants.ROLE_ADMIN)
    		.and()
    	.httpBasic()
    		.and()
    	.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/")
    		.invalidateHttpSession( true )
    		.deleteCookies("JSESSIONID")
            .permitAll()
            .and()
    	.csrf().disable();
    http
	    .sessionManagement()
	    .sessionCreationPolicy(SessionCreationPolicy.ALWAYS);

  }
  
}