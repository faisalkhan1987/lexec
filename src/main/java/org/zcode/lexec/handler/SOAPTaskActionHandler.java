package org.zcode.lexec.handler;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.common.ParamResolver;
import org.zcode.lexec.constants.CommonConstants;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.rest.StrategyController;
import org.zcode.lexec.wsx.engine.IWSExecutor;
import org.zcode.lexec.wsx.engine.WSExecutionFactory;
import org.zcode.lexec.wsx.vo.SOAPRequest;
import org.zcode.lexec.wsx.vo.SOAPResponse;

/**
 * @author Faisal_Khan01 Sep 26, 2016
 *
 */
@Component
public class SOAPTaskActionHandler implements ITaskActionHandler {

	static final Logger logger = Logger.getLogger(SOAPTaskActionHandler.class);
	
	@Autowired
	TaskActionLogger taskActionLogger;
	
	@Autowired
	ParamResolver paramResolver;
	
	@Override
	public void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {
		String username = task.getExecutionUser();
		SOAPRequest soapRequest = preProcessTaskAction((SOAPTask) task, teleEnv);
		
		taskActionLogger.logInput(soapRequest);
		taskActionLogger.logInfo(username, "# Executing Task - " + task);
		
		IWSExecutor<SOAPMessage> executor = WSExecutionFactory.getInstance().getExecutor(WSExecutionFactory.TYPE_SOAP);
		SOAPResponse sOAPResponse = null;
		try {
			SOAPMessage soapMessage = (SOAPMessage) executor.executeWS(soapRequest.getXmlRequest(),
					soapRequest.getEndPointUri());
			sOAPResponse = new SOAPResponse();
			sOAPResponse.setSoapMessage(soapMessage);
			taskActionLogger.logInfo(username, "# SOAPRequest : " + soapRequest);
			taskActionLogger.logOutput(username, "# SOAP Request triggered succesfully");
		} catch (Exception e) {
			logger.error("Exception occurred in SOAP call", e);
			throw new ExecutionException(e);
		}
	}

	/**
	 * Pre-processing will process the XML template to create the actual XML input 
	 * and also replaces the enviornment variable in the Endpoint URL
	 * @param task
	 * @param teleEnv
	 * @return
	 * @throws PreProcessingException
	 */
	private SOAPRequest preProcessTaskAction(SOAPTask task, String teleEnv) throws PreProcessingException {
		SOAPRequest soapRequest = new SOAPRequest();
		String xmlRequest = task.getWsRequest();

		List<String> inputParams = task.getInputParams();

		if(inputParams != null){
			for (String inputParam : inputParams) {
				String paramValue = paramResolver.resolveParamValue(inputParam);
				xmlRequest = xmlRequest.replace(inputParam, paramValue);
			}
			
			for (String inputParam : inputParams) {
				if(xmlRequest.contains(inputParam)){
					throw new PreProcessingException("No Input found for InputParam : " + inputParam);
				}
			}
		}
		String endPoint = task.getWsEndpoint();
		endPoint = endPoint.replace(CommonConstants.TELE_ENV, teleEnv);
		
		logger.debug(CommonConstants.TELE_ENV + " : " + teleEnv);
		logger.debug("EndPoint : " + endPoint);

		soapRequest.setEndPointUri(endPoint);
		soapRequest.setXmlRequest(xmlRequest);
		System.out.println("XML Request : " + xmlRequest);
		System.out.println("End Point : " + task.getWsEndpoint());
		return soapRequest;
	}
	

}
