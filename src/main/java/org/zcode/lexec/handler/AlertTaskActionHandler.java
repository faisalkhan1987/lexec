/**
 * 
 */
package org.zcode.lexec.handler;

import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.common.ParamResolver;
import org.zcode.lexec.domain.AlertTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;

/**
 * @author fkhan
 *
 */
@Component("alertTaskActionHandler")
public class AlertTaskActionHandler implements ITaskActionHandler {

	@Autowired
	TaskActionLogger taskActionLogger;
	
	@Autowired
	ParamResolver paramResolver;
	
	
	@Override
	public void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {
		AlertTask alertTask = (AlertTask) task;
		StringTokenizer strTokenizer = new StringTokenizer(alertTask.getMessage());
		StringBuffer alertMessageBuffer = new StringBuffer();
		while(strTokenizer.hasMoreTokens()){
			String token = strTokenizer.nextToken();
			if(token.startsWith("#")){
				token = paramResolver.resolveParamValue(token);
			}
			alertMessageBuffer.append(token);
			alertMessageBuffer.append(" ");
		}
		taskActionLogger.logAlert(task.getExecutionUser(), alertMessageBuffer.toString());
	}

}
