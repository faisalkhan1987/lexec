package org.zcode.lexec.handler;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.common.ParamResolver;
import org.zcode.lexec.constants.CommonConstants;
import org.zcode.lexec.constants.SQLConstants;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.SymbolTable;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.sqlx.dao.DSConfigDao;
import org.zcode.lexec.sqlx.engine.IQueryExecutor;
import org.zcode.lexec.sqlx.engine.QueryExecutionFactory;
import org.zcode.lexec.sqlx.engine.UpdateQueryExecutor;
import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.exception.SQLExecutionException;
import org.zcode.lexec.sqlx.vo.SQLConnectionParams;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.sqlx.vo.SQLRow;

/**
 * @author Faisal_Khan01
 * 
 */
@Component
public class SQLTaskActionHandler implements ITaskActionHandler {

	static final Logger logger = Logger.getLogger(SQLTaskActionHandler.class);
	
	@Autowired
	DSConfigDao dsConfigDao;

	@Autowired
	TaskActionLogger taskActionLogger;

	@Autowired
	ParamResolver paramResolver;

	private SQLExecRequest request = null;

	private SQLExecResponse response = null;

	private String username = null;

	@Override
	public void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {
//		System.out.println(task);
/*		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
		this.username = task.getExecutionUser();
		request = preProcessTaskAction((SQLTask) task, teleEnv);
		response = this.handleRequest(request);
		postProcessTaskAction(task);
	}

	/**
	 * @param sqlRequest
	 * @return
	 * @throws ExecutionException
	 */
	private SQLExecResponse handleRequest(SQLExecRequest sqlRequest) throws ExecutionException {
		this.request = sqlRequest;
		String query = request.getQuery();
		
		taskActionLogger.logInfo(username, "# Executing Query - " + query);

		IQueryExecutor qExecutor = QueryExecutionFactory.getInstance().getExecutor(query);

		SQLExecResponse response = null;
		try {
			response = qExecutor.execute(request);

//			if (request.getTransactionMode() == SQLConstants.TransactionMode.TRANSACTION_REQUIRED) {
//
//			} else if (qExecutor instanceof UpdateQueryExecutor) {
//				qExecutor.commit();
//			}
		} catch (SQLException e) {
			taskActionLogger.logError("# SQLException in executing Query - " + query);
			taskActionLogger.logError("# SQLException : " + e.getMessage());
			throw new SQLExecutionException(e);
		} catch (ConnectionException e) {
			taskActionLogger.logError("# ConnectionException in executing Query - " + query);
			taskActionLogger.logError("# ConnectionException : " + e.getMessage());
			throw new SQLExecutionException(e);
		} 

		return response;
	}

	/**
	 * @param sqlTask
	 * @param symbolTable
	 * @return
	 * @throws PreProcessingException
	 */
	private SQLExecRequest preProcessTaskAction(SQLTask sqlTask, String teleEnv) throws PreProcessingException {

		taskActionLogger.logInfo(null, "# Pre-Processing Task - " + sqlTask.getName());

		SQLExecRequest sqlExecRequest = new SQLExecRequest();
		String query = sqlTask.getQueryText();
		taskActionLogger.logInfo(null, "# Query Template : " + query);

		List<String> inputParams = sqlTask.getInputParams();

		if (inputParams != null) {
			for (String inputParam : inputParams) {
				String paramValue = paramResolver.resolveParamValue(inputParam);
				query = query.replace(inputParam, paramValue);
			}

			for (String inputParam : inputParams) {
				if(query.contains(inputParam)){
					throw new PreProcessingException("No Input found for InputParam : " + inputParam);
				}
			}

		}
		
		sqlExecRequest.setQuery(query);
		SQLConnectionParams connParams = new SQLConnectionParams(dsConfigDao.findByDsName(sqlTask.getDbName()));
		String connectionString = connParams.getConnectionString();
		if(connectionString != null && !connectionString.isEmpty() && connectionString.contains(CommonConstants.TELE_ENV)){
			connectionString = connectionString.replace(CommonConstants.TELE_ENV, teleEnv);
		}
		sqlExecRequest.setConnectionParams(connParams);

		return sqlExecRequest;
	}

	/**
	 * @param task
	 */
	private void postProcessTaskAction(Task task) {
		String outVariable = task.getOutParam();

		if (outVariable != null) {
			taskActionLogger.logInfo(null,
					"# Post-Processing Response : " + response + " - Added to SymbolTable as " + outVariable);
			SymbolTable.getInstance().put(outVariable, response);
			taskActionLogger.logOutput(response);
		} else {
			// do nothing
		}
		//printSQLResponse(response);
	}

	/**
	 * @param response
	 */
	/*private void printSQLResponse(SQLExecResponse response) {
		StringBuffer strBuffer = new StringBuffer();
		for (String columnName : response.getColumnNames()) {
			strBuffer.append(columnName + "\t");
		}
		if (strBuffer.length() > 0) {
			taskActionLogger.logAction(strBuffer.toString());
		}
		for (SQLRow row : response.getSqlRows()) {
			strBuffer = new StringBuffer();
			for (String columnValue : row.getColumnValues()) {
				strBuffer.append(columnValue + "\t");
			}
			if (strBuffer.length() > 0) {
				taskActionLogger.logAction(strBuffer.toString());
			}
		}
	}*/


}
