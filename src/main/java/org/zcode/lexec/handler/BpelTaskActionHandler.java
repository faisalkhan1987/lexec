/**
 * 
 */
package org.zcode.lexec.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.bpel.BPELConstants;
import org.zcode.lexec.bpel.BpelApiHandlerFactory;
import org.zcode.lexec.common.ParamResolver;
import org.zcode.lexec.domain.BpelTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.SymbolTable;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;

/**
 * @author fkhan
 *
 */
@Component("bpelTaskActionHandler")
public class BpelTaskActionHandler implements ITaskActionHandler {

	static final Logger logger = Logger.getLogger(BpelTaskActionHandler.class);

	//String userName = getUsername();
	
	String teleEnv = null;
	
	@Autowired
	TaskActionLogger taskActionLogger;
	
	@Autowired
	ParamResolver paramResolver;

	/* (non-Javadoc)
	 * @see org.zcode.lexec.selfcarev2.handler.ITaskActionHandler#handleTask(org.zcode.lexec.selfcarev2.domain.Task)
	 */
	@Override
	public void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {
		BpelTask bpelTask = (BpelTask)task;
		this.teleEnv = teleEnv;
		try {
			switch(bpelTask.getAction()){
			case RESUBMIT:
				resubmitBpelInstance(bpelTask);
				break;
			case CONTINUE:
				continueBpelInstance(bpelTask);
				break;
			case RECOVER:
				recoverBpelInstance(bpelTask);
				break;
			case ABORT:
				abortBpelInstance(bpelTask);
				break;
			case REPLACE_VARIABLE:
				replaceBpelVariableValue(bpelTask);
				break;
			case REPLACE_VARIABLE_INSERT:
				replaceBpelVariableInsertValue(bpelTask);
				break;
			case RETRY:
				retryBpelFault(bpelTask);
				break;
			case FETCH_FAULT_DETAILS:
				fetchFaultDetailsForBpelInstance(bpelTask);
				break;
			}
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			throw new ExecutionException(e);
		}
	}

	/**
	 * @return
	 */
	private String getUsername() {
		return paramResolver.resolveParamValue(BPELConstants.BPEL_USERNAME);
	}

	/**
	 * @param bpelTask
	 * @throws Exception 
	 */
	private void abortBpelInstance(BpelTask task) throws Exception {
		String userName = getUsername();
		taskActionLogger.logInfo(userName, "# abortBpelInstance " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
					getPassword(), instanceID).abortBPEL(instanceID);
		taskActionLogger.logInfo(userName,"# abortBpelInstance - succesful for " + task.toString());
	}

	/**
	 * @return
	 */
	private String getPassword() {
		return paramResolver.resolveParamValue(BPELConstants.BPEL_PASSWORD);
	}

	/**
	 * @param bpelTask
	 * @throws Exception 
	 */
	private void recoverBpelInstance(BpelTask task) throws Exception {
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# recoverBpelInstance " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
				getPassword(), instanceID).recoverBPEL(instanceID);
		taskActionLogger.logInfo(userName,"# recoverBpelInstance - succesful for " + task.toString());
	}

	private void resubmitBpelInstance(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# resubmitBpelInstance " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
				getPassword(), instanceID).resubmitBPEL(instanceID, SymbolTable.getInstance().getModifiedBpelVariables());
		taskActionLogger.logInfo(userName,"# resubmitBpelInstance - succesful for " + task.toString());
	}

	private void continueBpelInstance(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# continueBpelInstance " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
				getPassword(), instanceID).continueBPEL(instanceID);
		taskActionLogger.logInfo(userName,"# continueBpelInstance - succesful for " + task.toString());
	}

	private void fetchFaultDetailsForBpelInstance(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# fetchFaultDetailsForBpelInstance " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		Map<String, String> variables = BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
				getPassword(), instanceID).fetchDetails(instanceID);
//		if(variables.containsKey(BPELConstants.AUDIT_TRAIL)){
//			variables.remove(BPELConstants.AUDIT_TRAIL);
//		}
		if(variables.containsKey(BPELConstants.FAULT_TRACE)){
			variables.remove(BPELConstants.FAULT_TRACE);
		}
//		for(String key : variables.keySet()){
//			logger.debug(key + " : " + variables.get(key));
//		}
		SymbolTable.getInstance().putAllBpelVariables(variables);
		logger.debug(SymbolTable.getInstance().getAllBpelVariables().toString());
		taskActionLogger.logInfo(userName,"# fetchFaultForBpelInstance - succesful for " + task.toString());
	}

	private void replaceBpelVariableValue(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# replaceBpelVariableValue " + task.toString());
		
		if(SymbolTable.getInstance().getAllBpelVariables() == null || SymbolTable.getInstance().getAllBpelVariables().isEmpty()){
			fetchFaultDetailsForBpelInstance(task);
		}
		String bpelVariableName = task.getBpelVariableName();
		if(SymbolTable.getInstance().getAllBpelVariables() != null && !SymbolTable.getInstance().getAllBpelVariables().isEmpty()){
			String bpelVariableValue = (String)SymbolTable.getInstance().getBpelVariable(bpelVariableName);
			
			logger.debug(SymbolTable.getInstance().getAllBpelVariables().toString());
			logger.debug("BPEL Variable Name : " + bpelVariableName);
			logger.debug("BPEL Variable Value : " + bpelVariableValue);

			String sourceString = task.getSourceString();
			String targetString = task.getTargetString();

			logger.debug("Source String : " + sourceString);
			logger.debug("Target String : " + targetString);
			
			if(task.getInputParams() != null && !task.getInputParams().isEmpty()){
				for(String param : task.getInputParams()) {
					logger.debug("Param : " + param);
					if(sourceString.contains(param)){
						logger.debug("Source String : " + sourceString + " contains : " + param);
						sourceString = sourceString.replace(param, paramResolver.resolveParamValue(param));
					}			

					if(targetString.contains(param)){
						logger.debug("Target String : " + targetString + " contains : " + param);
						targetString = targetString.replace(param, paramResolver.resolveParamValue(param));
					}			
				}
			}
			
			
			bpelVariableValue = bpelVariableValue.replace(sourceString, targetString);
			
			SymbolTable.getInstance().putModifiedBpelVariable(bpelVariableName, bpelVariableValue);

			taskActionLogger.logOutput(userName,"# replaceBpelVariableValue - succesful for " + task.toString());
		}else{
			taskActionLogger.logAlert(userName,"# replaceBpelVariableValue - No Bpel Variables found to replace ");
		}
		
		
	}
	
	private void replaceBpelVariableInsertValue(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# replaceBpelVariableInsertValue " + task.toString());
		if(SymbolTable.getInstance().getAllBpelVariables() == null || SymbolTable.getInstance().getAllBpelVariables().isEmpty()){
			fetchFaultDetailsForBpelInstance(task);
		}

		if(SymbolTable.getInstance().getAllBpelVariables() != null && !SymbolTable.getInstance().getAllBpelVariables().isEmpty()){
			String bpelVariableName = task.getBpelVariableName();
			String bpelVariableValue = (String)SymbolTable.getInstance().getBpelVariable(bpelVariableName);
			
			logger.debug(SymbolTable.getInstance().getAllBpelVariables().toString());
			logger.debug("BPEL Variable Name : " + bpelVariableName);
			logger.debug("BPEL Variable Value : " + bpelVariableValue);
			
			String insertAfterNode = task.getInsertAfterNode();
			String newNodeString = task.getNewNode();
			logger.debug("Insert After Node : " + insertAfterNode);
			logger.debug("New Node : " + newNodeString);
	
			if(task.getInputParams() != null && !task.getInputParams().isEmpty()){
				for(String param : task.getInputParams()) {
					logger.debug("Param : " + param);
					if(newNodeString.contains(param)){
						logger.debug("newNodeString : " + newNodeString + " contains : " + param);
						newNodeString = newNodeString.replace(param, paramResolver.resolveParamValue(param));
					}
					if(insertAfterNode.contains(param)){
						logger.debug("InsertAfterNode : " + insertAfterNode + " contains : " + param);
						insertAfterNode = insertAfterNode.replace(param, paramResolver.resolveParamValue(param));
					}
				}
			}
	
			taskActionLogger.logInfo(userName,"# Insert " + newNodeString + " after : "+insertAfterNode);
			
			StringBuilder strBuilder = new StringBuilder(bpelVariableValue);
			
			int startIndex = strBuilder.indexOf(insertAfterNode);
			int endIndex = startIndex + insertAfterNode.length();
			
			strBuilder.insert(endIndex, newNodeString);
			
			SymbolTable.getInstance().putModifiedBpelVariable(bpelVariableName, strBuilder.toString());
			
			taskActionLogger.logOutput(userName,"# replaceBpelVariableInsertValue - succesful for " + task.toString());
		}else{
			taskActionLogger.logAlert(userName,"# replaceBpelVariableInsertValue - No Bpel Variables found");
		}
	} 
	
	private void retryBpelFault(BpelTask task) throws Exception{
		String userName = getUsername();
		taskActionLogger.logInfo(userName,"# retryBpelFault " + task.toString());
		String instanceID = paramResolver.resolveParamValue(task.getInstanceVariable());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", teleEnv);
		BpelApiHandlerFactory.getBPELApiHandler(providerURL, userName,
				getPassword(), instanceID).retryBPEL(instanceID,SymbolTable.getInstance().getModifiedBpelVariables());
		taskActionLogger.logInfo(userName,"# retryBpelFault - succesful for " + task.toString());
	}
}
