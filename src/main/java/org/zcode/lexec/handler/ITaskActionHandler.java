package org.zcode.lexec.handler;

import java.util.Map;

import org.zcode.lexec.domain.Task;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;



/**
 * @author Faisal_Khan01
 * This is the interface class that every Task Action Handler should implement
 * For example, SQLTaskActionHandler, WSTaskActionHandler, BPELTaskActionHandler, etc
 * Request and Response are generic types that can be decided when implement the Execution engine 
 */
public interface ITaskActionHandler {

	/**
	 * The implementation should execute the desired functionality using the parameters provided in the request
	 * and provide the result in the response object
	 * @param request
	 * @return
	 
	Response handleRequest(Request request) throws ExecutionException;
	*/
	/**
	 * @param task
	 * @param teleEnv
	 * @return
	 */
	void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException;
}
