package org.zcode.lexec.handler;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.antlr4.CustomListener;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarLexer;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarParser;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.factory.TaskHandlerFactory;

/**
 * @author faisal_khan01 Oct 5, 2016
 *
 */
@Component
public class CTaskActionHandler implements ITaskActionHandler {

	@Autowired
	CustomListener listener;
	
	@Autowired
	TaskHandlerFactory taskHandlerFactory;

	@Autowired
	TaskActionLogger taskActionLogger;
	 
	@Override
	public void handleTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {

		CTask cTask = (CTask) task;
		
		boolean executeChildTasks = false;
		
		
		if(cTask.getExpression() == null || cTask.getExpression().isEmpty()){
			executeChildTasks = true;
		} else {
			executeChildTasks = evaluateExpression(cTask.getExpression());
		}
		if(executeChildTasks){
			for (Task childTask : cTask.getChildTasks()) {
				taskHandlerFactory.processTask(childTask, teleEnv);
				try{
					Thread.sleep(task.getSleepTime());
				} catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	private boolean evaluateExpression(String expression){
		boolean result = false;
		CharStream in = new ANTLRInputStream(expression);		
		RuleSetGrammarLexer lexer = new RuleSetGrammarLexer(in);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);	
		RuleSetGrammarParser parser = new RuleSetGrammarParser(tokenStream);
		new ParseTreeWalker().walk(listener, parser.rule_set());
		result = listener.getResult();
		taskActionLogger.logInfo(null, "# Expression : "+ expression + " evaluated to value : " + result);
		return result;
	}
	
}
