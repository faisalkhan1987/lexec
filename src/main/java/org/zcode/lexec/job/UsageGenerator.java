/**
 * 
 */
package org.zcode.lexec.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author fkhan
 *
 */
@Component
public class UsageGenerator {

	static final Logger logger = Logger.getLogger(UsageGenerator.class);

//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 5000000)
    public void reportCurrentTime() {
//		logger.info("The time is now {" + dateFormat.format(new Date()) + "}");
//		System.out.println("reportCurrentTime");
    }
}
