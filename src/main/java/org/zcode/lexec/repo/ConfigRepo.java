/**
 * 
 */
package org.zcode.lexec.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.Config;

/**
 * @author fkhan
 *
 */
public interface ConfigRepo extends MongoRepository<Config, String>{

}
