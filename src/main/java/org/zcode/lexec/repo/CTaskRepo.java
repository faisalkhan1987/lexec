package org.zcode.lexec.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.CTask;

/**
 * @author Faisal_Khan01
 * Sep 23, 2016
 *
 */
public interface CTaskRepo extends MongoRepository<CTask, String>{

    List<CTask> findByNameIgnoreCaseLikeOrDescriptionIgnoreCaseLike(String name, String description);
    
    CTask findByName(String name);
    
    List<CTask> findByGroup(String groupName);
    
}
