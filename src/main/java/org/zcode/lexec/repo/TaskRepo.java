package org.zcode.lexec.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.Task;

/**
 * @author Faisal_Khan01
 * Sep 29, 2016
 *
 */
public interface TaskRepo extends MongoRepository<Task, String>{

}
