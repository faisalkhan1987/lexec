package org.zcode.lexec.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.TaskLog;

/**
 * @author Faisal_Khan01
 * Oct 20, 2016
 *
 */
public interface TaskLogRepo extends MongoRepository<TaskLog, String>{

	List<TaskLog> findByThreadId(String threadId);
	
	List<TaskLog> findDistinctThreadIdByLogLike(String text);
	
	List<TaskLog> findDistinctThreadIdByUser(String user);
}
