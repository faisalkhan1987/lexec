/**
 * 
 */
package org.zcode.lexec.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.Report;

/**
 * @author fkhan
 *
 */
public interface ReportsRepo extends MongoRepository<Report, String>{

}
