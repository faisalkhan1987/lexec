/**
 * 
 */
package org.zcode.lexec.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.User;

/**
 * @author fkhan
 *
 */
public interface UserRepo extends MongoRepository<User, String>{

	User findByUsername(String username);
}
