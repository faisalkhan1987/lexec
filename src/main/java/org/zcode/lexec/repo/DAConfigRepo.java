/**
 * 
 */
package org.zcode.lexec.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.DataAnalysisConfig;

/**
 * @author fkhan
 *
 */
public interface DAConfigRepo  extends MongoRepository<DataAnalysisConfig, String>{

	DataAnalysisConfig findByInputType(String inputType);
}
