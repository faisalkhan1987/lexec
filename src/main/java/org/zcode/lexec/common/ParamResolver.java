package org.zcode.lexec.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.engine.SymbolTable;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.sqlx.vo.SQLRow;

/**
 * @author faisal_khan01 Oct 5, 2016
 * 
 */
@Component
public class ParamResolver {

    @Autowired
    TaskActionLogger taskActionLogger;
    
    /**
     * @param inputParam
     * @return
     * @throws Exception
     */
	public String resolveParamValue(String inputParam) {

//		taskActionLogger.logInfo(null,"# Resolve Param : "+ inputParam);
		
		if (inputParam.startsWith("#")) {
	
		    Object valueObj = SymbolTable.getInstance().get(
			    stripOffPositionCoords(inputParam));
	
		    if (valueObj instanceof String) {
		    	return resolveParamValue((String) valueObj);
		    } else if (valueObj instanceof SQLExecResponse) {
				SQLExecResponse response = (SQLExecResponse) valueObj;
				
				String value = null;
				Pair xyCoords = new Pair(0, 0);
				if (inputParam.contains("[")) {
				    xyCoords = getXYCoordinates(inputParam);
				    if (response.getSqlRows().size() <= xyCoords.x) {			    
					    taskActionLogger.logError("Param value for "
							    + inputParam + " does not have the value at X "
							    + xyCoords.x);
					}
			
					SQLRow sqlRow = response.getSqlRows().get(xyCoords.x);
					if (sqlRow.getColumnValues().size() <= xyCoords.y) {
						taskActionLogger.logError("Param value for "
						    + inputParam + " does not have the value at Y "
						    + xyCoords.y);
					}
					value = sqlRow.getColumnValues().get(xyCoords.y);
				}else {
					StringBuffer strBuffer = new StringBuffer();
					for(SQLRow sqlRow : response.getSqlRows()){
						strBuffer.append(sqlRow.getColumnValues().get(0)).append(",");
					}
					strBuffer.deleteCharAt(strBuffer.length()-1);
					value = strBuffer.toString();
				}
		
				
//				taskActionLogger.logInfo(null,"# Param : "+ inputParam + " resolved to Value : " + value);
				return value;
		    }
		}
		return inputParam;
    }

	
//    public static void main(String[] args) {
//	ParamResolver pr = new ParamResolver();
//	System.out.println(pr.stripOffPositionCoords("#Param"));
//	System.out.println(pr.stripOffPositionCoords("#Param[0][1]"));
//    }

    private String stripOffPositionCoords(String inputParam) {
	if (inputParam.indexOf('[') != -1)
	    return inputParam.substring(0, inputParam.indexOf('['));
	else
	    return inputParam;
    }

    private Pair getXYCoordinates(String inputParam) {
	int beginIndex = inputParam.indexOf('[');
	int endIndex = inputParam.indexOf(']');
	String xCoord = inputParam.substring(beginIndex + 1, endIndex);
	beginIndex = inputParam.indexOf('[', endIndex);
	endIndex = inputParam.indexOf(']', beginIndex);
	String yCoord = inputParam.substring(beginIndex + 1, endIndex);
	return new Pair(Integer.parseInt(xCoord), Integer.parseInt(yCoord));
    }

    class Pair {
	int x;
	int y;

	Pair(int x, int y) {
	    this.x = x;
	    this.y = y;
	}
    }
}
