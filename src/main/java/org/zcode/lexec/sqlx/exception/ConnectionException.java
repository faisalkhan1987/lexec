package org.zcode.lexec.sqlx.exception;
/**
 * @author faisal_khan01
 *
 */
public class ConnectionException extends Throwable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8577233446228024064L;

	public ConnectionException(Throwable th){
		super(th);
	}

	public ConnectionException(String string) {
		super(string);
	}
}
