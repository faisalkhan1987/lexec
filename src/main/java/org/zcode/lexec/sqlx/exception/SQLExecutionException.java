package org.zcode.lexec.sqlx.exception;

import org.zcode.lexec.exception.ExecutionException;

/**
 * @author Faisal_Khan01
 *
 */
public class SQLExecutionException extends ExecutionException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7969004776222813885L;

	public SQLExecutionException(Throwable e) {
		super(e);
	}

}
