package org.zcode.lexec.sqlx.engine;

import java.sql.SQLException;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;


/**
 * @author Faisal_Khan01
 * Executor that executes only SELECT queries
 */
public class SelectQueryExecutor extends AbstractQueryExecutor{

	/* (non-Javadoc)
	 * @see org.lexec.sql.IQueryExecutor#execute(org.lexec.sql.vo.SQLExecutionRequest)
	 */
	public SQLExecResponse execute(SQLExecRequest request) throws ConnectionException, SQLException {
		
		SQLExecResponse response = null;
		conn = getConnection(request.getConnectionParams());
		try{
			pstmt = conn.prepareStatement(request.getQuery());
			rs = pstmt.executeQuery();
			
			response = SQLResponseBuilder.getInstance().buildResponse(rs);
		} finally {
			closeAllResources();
		}
		
		return response;
	}

	/* (non-Javadoc)
	 * @see org.lexec.sql.utils.IQueryExecutor#commit()
	 */
	public void commit() throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
