package org.zcode.lexec.sqlx.engine;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLConnectionParams;


/**
 * @author Faisal_Khan01
 *
 */
public abstract class AbstractQueryExecutor implements IQueryExecutor{

	protected Connection conn;
	
	protected PreparedStatement pstmt;
	
	protected ResultSet rs;
	
	protected ResultSetMetaData rsmd;
	
	protected CallableStatement callStmt;
	/**
	 * @param connectionParams
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	protected Connection getConnection(SQLConnectionParams connectionParams) throws ConnectionException, SQLException {
		conn = DBConnectionManager.getInstance().getConnection(connectionParams);
		if(conn == null){
			throw new ConnectionException("Connection is Null");
		}else{
			conn.setAutoCommit(false);
		}
		return conn;
	}
	
	/**
	 * 
	 */
	public void closeAllResources(){
		closeRS();
		closePSTMT();
		closeCSTMT();
		closeConnection();
	}
	
	/**
	 * 
	 */
	protected void closeCSTMT() {
		try {
			if(callStmt != null && !callStmt.isClosed()){
				callStmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	protected void closeRS() {
		try {
			if(rs != null && !rs.isClosed()){
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	protected void closePSTMT(){
		try {
			if(pstmt != null && !pstmt.isClosed()){
				pstmt.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * 
	 */
	protected void closeConnection(){
		try {
			if(conn != null && !conn.isClosed()){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
