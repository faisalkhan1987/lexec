package org.zcode.lexec.sqlx.engine;

import java.sql.SQLException;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;


/**
 * @author Faisal_Khan01
 *
 */
public class UpdateQueryExecutor extends AbstractQueryExecutor{

	/* (non-Javadoc)
	 * @see org.lexec.sql.IQueryExecutor#execute(org.lexec.sql.vo.SQLExecutionRequest)
	 */
	public SQLExecResponse execute(SQLExecRequest request) throws ConnectionException, SQLException {
		SQLExecResponse response = new SQLExecResponse();
		try{
			conn = getConnection(request.getConnectionParams());
			
			pstmt = conn.prepareStatement(request.getQuery());
			int nRowsAffected = pstmt.executeUpdate();
			
			response.setnRowsAffected(nRowsAffected);
			conn.commit();
		}finally{
			closeAllResources();
		}		
		return response;
	}

	/* (non-Javadoc)
	 * @see org.lexec.sql.utils.IQueryExecutor#commit()
	 */
	public void commit() throws SQLException {
		
	}

}
