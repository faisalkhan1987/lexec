package org.zcode.lexec.sqlx.engine;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;

import oracle.jdbc.OracleTypes;

/**
 * @author faisal_khan01
 *
 */
public class SQLProcedureExecutor extends AbstractQueryExecutor{

	/* (non-Javadoc)
	 * @see org.lexec.sql.utils.IQueryExecutor#execute(org.lexec.sql.vo.SQLExecutionRequest)
	 */
	public SQLExecResponse execute(SQLExecRequest request)
			throws ConnectionException, SQLException {
		
		SQLExecResponse response = null;
		try{
			conn = getConnection(request.getConnectionParams());
			
			callStmt = conn.prepareCall(request.getQuery());
			
			callStmt.registerOutParameter(1, OracleTypes.CURSOR);
			
			callStmt.execute();
			
			rs = (ResultSet) callStmt.getObject(1);
			
			if(rs != null){
				response = SQLResponseBuilder.getInstance().buildResponse(rs);
			}
		} finally{
			closeAllResources();
		}
		
		return response;
	}

	/* (non-Javadoc)
	 * @see org.lexec.sql.utils.IQueryExecutor#commit()
	 */
	public void commit() throws SQLException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
