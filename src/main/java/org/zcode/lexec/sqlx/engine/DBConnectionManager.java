package org.zcode.lexec.sqlx.engine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLConnectionParams;

/**
 * @author faisal_khan01
 * 
 */
public class DBConnectionManager {

    private static DBConnectionManager singletonInstance;

    /**
     * @return
     */
    public static final DBConnectionManager getInstance() {
	if (singletonInstance == null) {
	    singletonInstance = new DBConnectionManager();
	}
	return singletonInstance;
    }

    public Connection getConnection(SQLConnectionParams connectionParams)
	    throws ConnectionException {
	if (connectionParams.getContextPath() != null
		&& !connectionParams.getContextPath().isEmpty()) {
	    return getJndiConnection(connectionParams);
	} else {
	    return getJDBCConnection(connectionParams);
	}
    }

    /**
     * @param connectionParams
     * @return
     * @throws ConnectionException
     */
    private Connection getJndiConnection(SQLConnectionParams connectionParams)
	    throws ConnectionException {
	Connection conn = null;
	System.out.println("** getJndiConnection - " + connectionParams.getContextPath());
	try {
	    Context initialContext = new InitialContext();

	    DataSource ds = (DataSource) initialContext.lookup(connectionParams
		    .getContextPath());
	    if (ds == null) {
		throw new ConnectionException("DataSource "
			+ connectionParams.getContextPath() + " is NULL");
	    }
	    conn = ds.getConnection();
	} catch (NamingException e) {
		e.printStackTrace();
	    throw new ConnectionException(e);
	} catch (SQLException e) {
		e.printStackTrace();
	    throw new ConnectionException(e);
	}

	return conn;
    }

    /**
     * @param connectionParams
     * @return
     * @throws ConnectionException
     */
    private Connection getJDBCConnection(SQLConnectionParams connectionParams)
	    throws ConnectionException {

	Connection conn = null;

	try {
	    Class.forName(connectionParams.getDriverClassName());
	    conn = DriverManager.getConnection(
		    connectionParams.getConnectionString(),
		    connectionParams.getUserName(),
		    connectionParams.getPassword());
	} catch (SQLException e) {
	    throw new ConnectionException(e);
	} catch (ClassNotFoundException e) {
	    throw new ConnectionException(e);
	}

	return conn;
    }
}
