package org.zcode.lexec.sqlx.engine;

/**
 * @author Faisal_Khan01 Factory that determines which QueryExecutor to be
 *         created for the query to be executed
 */
public class QueryExecutionFactory {

    private static QueryExecutionFactory singletonInstance = null;

    /**
     * @return
     */
    public static final QueryExecutionFactory getInstance() {
	if (singletonInstance == null) {
	    singletonInstance = new QueryExecutionFactory();
	}
	return singletonInstance;
    }

    /**
     * @param query
     * @return
     */
    public IQueryExecutor getExecutor(String query) {
	if (query.toLowerCase().startsWith("update")
		|| query.toLowerCase().startsWith("delete")
		|| query.toLowerCase().startsWith("insert")) {
	    return new UpdateQueryExecutor();
	} else if (query.toLowerCase().startsWith("begin")) {
	    return new SQLProcedureExecutor();
	} else {
	    return new SelectQueryExecutor();
	}
    }
}
