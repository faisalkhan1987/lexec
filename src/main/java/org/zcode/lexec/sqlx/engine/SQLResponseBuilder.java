package org.zcode.lexec.sqlx.engine;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.sqlx.vo.SQLRow;


/**
 * @author faisal_khan01
 *
 */
public class SQLResponseBuilder {

	private static SQLResponseBuilder sharedInstance;
	
	/**
	 * @return
	 */
	public static final SQLResponseBuilder getInstance(){
		if(sharedInstance == null){
			sharedInstance = new SQLResponseBuilder();
		}
		return sharedInstance;
	}
	
	public SQLExecResponse buildResponse(ResultSet rs) throws SQLException{
		
		SQLExecResponse response = new SQLExecResponse();
		
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();
		
		for (int col = 1; col <= columnCount; col++) {
			response.addColumnName(rsmd.getColumnName(col));
		}
		
		while(rs.next()){
			
			SQLRow row = new SQLRow();
			
			for (int col = 1; col <= columnCount; col++) {

				row.addColumnValue(rs.getString(col));
			}
			
			response.addSQLRow(row);
			
		}
		
		return response;
	}
}
