package org.zcode.lexec.sqlx.engine;

import java.sql.SQLException;

import org.zcode.lexec.sqlx.exception.ConnectionException;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;

/**
 * @author Faisal_Khan01
 *
 */
public interface IQueryExecutor {

	/**
	 * @param request
	 * @return
	 * @throws ConnectionException
	 * @throws SQLException
	 */
	SQLExecResponse execute(SQLExecRequest request) throws ConnectionException, SQLException ;

	/**
	 * 
	 */
	void commit() throws SQLException;
}
