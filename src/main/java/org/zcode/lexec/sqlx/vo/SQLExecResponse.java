package org.zcode.lexec.sqlx.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Faisal_Khan01
 *
 */
public class SQLExecResponse {

	private List<String> columnNames;
	private List<SQLRow> sqlRows;

	private int nRowsAffected;
	/**
	 * @param row
	 */
	public void addSQLRow(SQLRow row){
		getSqlRows().add(row);
	}
	/**
	 * @return the sqlRows
	 */
	public List<SQLRow> getSqlRows() {
		if(this.sqlRows == null){
			this.sqlRows = new ArrayList<SQLRow>();
		}
		return sqlRows;
	}

	/**
	 * @param sqlRows the sqlRows to set
	 */
	public void setSqlRows(List<SQLRow> sqlRows) {
		this.sqlRows = sqlRows;
	}
	
	/**
	 * @param columnName
	 */
	public void addColumnName(String columnName){
		getColumnNames().add(columnName);
	}
	/**
	 * @return the columnNames
	 */
	public List<String> getColumnNames() {
		if(this.columnNames == null){
			this.columnNames = new ArrayList<String>();
		}
		return columnNames;
	}
	/**
	 * @param columnNames the columnNames to set
	 */
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}
	/**
	 * @return the nRowsAffected
	 */
	public int getnRowsAffected() {
		return nRowsAffected;
	}
	/**
	 * @param nRowsAffected the nRowsAffected to set
	 */
	public void setnRowsAffected(int nRowsAffected) {
		this.nRowsAffected = nRowsAffected;
	}
	
	
}
