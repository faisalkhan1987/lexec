package org.zcode.lexec.sqlx.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Faisal_Khan01
 *
 */
public class SQLRow {

	private List<String> columnValues;

	/**
	 * @param value
	 */
	public void addColumnValue(String value){
		getColumnValues().add(value);
	}
	/**
	 * @return the columnValues
	 */
	public List<String> getColumnValues() {
		if(this.columnValues == null){
			this.columnValues = new ArrayList<String>();
		}
		return columnValues;
	}

	/**
	 * @param columnValues the columnValues to set
	 */
	public void setColumnValues(List<String> columnValues) {
		this.columnValues = columnValues;
	}
	
	
}
