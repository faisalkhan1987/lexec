package org.zcode.lexec.sqlx.vo;

import org.zcode.lexec.domain.DSConfig;

/**
 * @author Faisal_Khan01
 * 
 */
public class SQLConnectionParams {

    private String userName;

    private String password;

    private String driverClassName;

    private String connectionString;

    private String contextPath;

    public SQLConnectionParams() {
    }

    public SQLConnectionParams(DSConfig dsConfig) {
	this.userName = dsConfig.getUserName();
	this.password = dsConfig.getEncryptedPassword();
	this.driverClassName = dsConfig.getDriverClassName();
	this.contextPath = dsConfig.getContextPath();
	this.connectionString = dsConfig.getJdbcConnectionString();
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
	return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * @return the driveClassName
     */
    public String getDriverClassName() {
	return driverClassName;
    }

    /**
     * @param driveClassName
     *            the driveClassName to set
     */
    public void setDriverClassName(String driverClassName) {
	this.driverClassName = driverClassName;
    }

    /**
     * @return the connectionString
     */
    public String getConnectionString() {
	return connectionString;
    }

    /**
     * @param connectionString
     *            the connectionString to set
     */
    public void setConnectionString(String connectionString) {
	this.connectionString = connectionString;
    }

    /**
     * @return the contextPath
     */
    public String getContextPath() {
	return contextPath;
    }

    /**
     * @param contextPath
     *            the contextPath to set
     */
    public void setContextPath(String contextPath) {
	this.contextPath = contextPath;
    }
}
