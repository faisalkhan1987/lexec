package org.zcode.lexec.sqlx.vo;

import org.zcode.lexec.constants.SQLConstants.TransactionMode;


/**
 * @author Faisal_Khan01
 *
 */
public class SQLExecRequest {

	private SQLConnectionParams connectionParams;
	
	private String query;
	
	private TransactionMode transactionMode;

	/**
	 * @return the connectionParams
	 */
	public SQLConnectionParams getConnectionParams() {
		return connectionParams;
	}

	/**
	 * @param connectionParams the connectionParams to set
	 */
	public void setConnectionParams(SQLConnectionParams connectionParams) {
		this.connectionParams = connectionParams;
	}

	/**
	 * @return the transactionMode
	 */
	public TransactionMode getTransactionMode() {
		return transactionMode;
	}

	/**
	 * @param transactionMode the transactionMode to set
	 */
	public void setTransactionMode(TransactionMode transactionMode) {
		this.transactionMode = transactionMode;
	}

	/**
	 * @return the query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	
}
