package org.zcode.lexec.sqlx.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.zcode.lexec.domain.DSConfig;

/**
 * @author faisal_khan01
 *
 */
public interface DSConfigDao extends MongoRepository<DSConfig, String> {

    DSConfig findByDsName(String dsName);
}
