package org.zcode.lexec.sqlx.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.SQLTask;

/**
 * @author faisal_khan01
 *
 */
public interface SQLTaskDao extends MongoRepository<SQLTask, String> {

    SQLTask findByName(String name);
}
