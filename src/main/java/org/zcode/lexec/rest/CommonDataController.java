/**
 * 
 */
package org.zcode.lexec.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.constants.CommonConstants;
import org.zcode.lexec.domain.Config;
import org.zcode.lexec.domain.DSConfig;
import org.zcode.lexec.domain.Report;
import org.zcode.lexec.domain.TaskLog;
import org.zcode.lexec.domain.User;
import org.zcode.lexec.json.UsageStatistics;
import org.zcode.lexec.repo.ConfigRepo;
import org.zcode.lexec.repo.ReportsRepo;
import org.zcode.lexec.repo.TaskLogRepo;
import org.zcode.lexec.repo.UserRepo;
import org.zcode.lexec.sqlx.dao.DSConfigDao;

/**
 * @author fkhan
 *
 */
@Component
@Path("/")
@Produces("application/json")
public class CommonDataController {

	@Autowired
	DSConfigDao dsConfigDao;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	TaskLogRepo taskLogRepo;
	
	@Autowired ReportsRepo reportsRepo;

	@Autowired ConfigRepo configRepo;
/*	
	@GET
	@Path("/fetchUsageStatistics")
	public UsageStatistics fetchUsageStatistics(){
		
		List<Report> reports = reportsRepo.findAll();
		List<User> users = userRepo.findAll();
		UsageStatistics statistics = new UsageStatistics();

		for(Report report : reports){
			List<TaskLog> taskLogList = taskLogRepo.findDistinctThreadIdByLogLike(report.getSearchText());
			if(taskLogList != null){
				statistics.getWorkaroundStatistics().put(report.getReportName(), taskLogList.size());
			}else{
				statistics.getWorkaroundStatistics().put(report.getReportName(), 0);
			}
		}
		
		for(User user : users){
			List<TaskLog> taskLogList = taskLogRepo.findDistinctThreadIdByUser(user.getUsername());
			if(taskLogList != null){
				statistics.getUserStatistics().put(user.getUsername(), taskLogList.size());
			}else{
				statistics.getWorkaroundStatistics().put(user.getUsername(), 0);
			}
		}
		return statistics;
	}
	*/
	
	@GET
	@Path("/getAllGroups")
	public List<String> getAllGroups() {
		List<Config> configList = configRepo.findAll();
		return configList.get(0).getGroups();
	}
	@GET
	@Path("/getAllEnvs")
	public List<String> getAllEnvs() {
		List<String> envs = new ArrayList<>();
		envs.add("PRD");
		envs.add("TRG");
		envs.add("UAT");
		envs.add("SUP");
		envs.add("INT");
		envs.add("EDP");
		envs.add("DVP");
		return envs;
	}
	
	@GET
	@Path("/getAllDbs")
	public List<String> getAllDbs(){
		List<String> dbs = new ArrayList<String>();
		
		List<DSConfig> dsConfigs = dsConfigDao.findAll();
		for(DSConfig dsConfig : dsConfigs){
			dbs.add(dsConfig.getDsName());
		}
		
		return dbs;
	}
	
	@GET
	@Path("/getUserEnvs")
	public Map getBpelEnvsForUser(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepo.findByUsername(username);
		Map map = new HashMap();
		map.put("bpelEnvs", user.getBpelEnvs());
		map.put("teleEnv", user.getTeleEnv());
		return map;
	}

/*	@GET
	@Path("/getTeleEnvForUser")
	public String getTeleEnvForUser(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepo.findByUsername(username);
		return user.getTeleEnv();
	} */
	
	@GET
	@Path("/isAdminRole")
	public boolean isAdminRole(){
		boolean isAdmin = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		String[] userRoles = userRepo.findByUsername(username).getRoles();
		for (String role : userRoles){
			if(CommonConstants.ROLE_ADMIN.equals(role)){
				isAdmin = true; break;
			}
		}
		return isAdmin;
	}
	
	@GET
	@Path("/isSuperAdminRole")
	public boolean isSuperAdminRole(){
		boolean isAdmin = false;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		String[] userRoles = userRepo.findByUsername(username).getRoles();
		for (String role : userRoles){
			if(CommonConstants.ROLE_SUPER_ADMIN.equals(role)){
				isAdmin = true; break;
			}
		}
		return isAdmin;
	}
	@GET
	@Path("/getUserRoles")
	public String[] getUserRoles(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		return userRepo.findByUsername(username).getRoles();
	}
}
