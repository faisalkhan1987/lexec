package org.zcode.lexec.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.DataAnalysisConfig;
import org.zcode.lexec.domain.TaskLog;
import org.zcode.lexec.domain.User;
import org.zcode.lexec.engine.ITaskActionOrchestrator;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.factory.TaskBuilderFactory;
import org.zcode.lexec.json.BulkTaskExecutionRequest;
import org.zcode.lexec.json.ExecResponse;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.TaskExecutionRequest;
import org.zcode.lexec.repo.CTaskRepo;
import org.zcode.lexec.repo.DAConfigRepo;
import org.zcode.lexec.repo.TaskLogRepo;
import org.zcode.lexec.repo.UserRepo;

/**
 * @author Faisal_Khan01 Oct 13, 2016
 * 
 */
@Component
@Path("/strategy")
@Produces("application/json")
public class StrategyController {

	static final Logger logger = Logger.getLogger(StrategyController.class);
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	TaskBuilderFactory taskBuilderFactory;

	@Autowired
	CTaskRepo cTaskRepo;

	@Autowired
	TaskLogRepo taskLogRepo;
	
	@Autowired
	DAConfigRepo daConfigRepo;
	
	@Autowired
	TaskActionLogger taskActionLogger;

	@Resource
	ITaskActionOrchestrator customTaskActionOrchestrator;

	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse saveStrategy(Strategy strategy) {
		ExecResponse response = new ExecResponse();
		CTask cTask = (CTask) taskBuilderFactory.buildTask(strategy);
		cTaskRepo.save(cTask);
		return response;
	}

	@POST
	@Path("/execute")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse executeTask(TaskExecutionRequest request) {
		logger.debug("executeTask : " + request);
		ExecResponse response = new ExecResponse();
		CTask cTask = cTaskRepo.findOne(request.getTaskId());
		if(cTask.isApproved()){
			List<String> inputParams = cTask.getInputParams();
			
			Map<String, String> inputMap = new HashMap<String, String>();
			int index = 0;
			// Iterate through input params provided in the template
			for (String inputParam : inputParams) {
				// Put the param values from request into the inputMap
				inputMap.put(inputParam, request.getInputParams().get(index++));
			}

			List<String> passwordParams = cTask.getPasswordParams();
			index = 0;
			// Iterate through input params provided in the template
			if(passwordParams != null){
				for (String passwordParam : passwordParams) {
					// Put the param values from request into the inputMap
					inputMap.put(passwordParam, request.getPasswordParams().get(index++));
				}
			}
			
			cTask.setExecutionUser(getUsername());
			logger.debug("customTaskActionOrchestrator : " + customTaskActionOrchestrator);
//			System.out.println("******* CURRENT PRINCIPAL NAME : " + username);
			customTaskActionOrchestrator.execute(inputMap, cTask, response.getThreadId(), getTeleEnv());

			response.setStatus("SUCCESS");
		} else {
			response.setErrorMessage("This Workaround is not approved");
			response.setStatus("FAILURE");
		}
		
		return response;
	}

	@POST
	@Path("/executeBulk")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse executeBulkTask(BulkTaskExecutionRequest request) {
		logger.debug("executeTask : " + request);
		ExecResponse response = new ExecResponse();
		CTask cTask = cTaskRepo.findOne(request.getTaskId());
		
		if(cTask.isApproved() && cTask.isApprovedForBulkExec()){
			String inputString = request.getInputString();
			String[] inputList = inputString.split("\n");
			customTaskActionOrchestrator.bulkAsyncExecute(inputList, cTask, response.getThreadId(), 
					request.getSleepTime(), getTeleEnv(), getUsername());

			response.setStatus("SUCCESS");
		} else {
			response.setErrorMessage("This Workaround is not approved or not approved for Bulk execution");
			response.setStatus("FAILURE");
		}
		
		return response;
	}

	private String getUsername(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		logger.debug("getUsername : " + authentication);
		return authentication.getName();		
	}
	/**
	 * @return
	 */
	private String getTeleEnv() {
		User user = userRepo.findByUsername(getUsername());
		return user.getTeleEnv();
	}

	@GET
	@Path("/triggerDataAnalysis")
	public ExecResponse triggerDataAnalysis(@QueryParam("inputType") String inputType, 
			@QueryParam("inputValue") String inputValue) {
		logger.debug("triggerDataAnalysis : inputType=" + inputType + ", inputValue="+inputValue);
		ExecResponse response = new ExecResponse();
		DataAnalysisConfig daConfig = daConfigRepo.findByInputType(inputType);
		CTask cTask = cTaskRepo.findByName(daConfig.getStrategyName());
		
		if(cTask.isApproved()){
			// Creating a new ObjectId which will be used as a reference for the logger
			Map<String, String> inputMap = new HashMap<String, String>();
			inputMap.put("#"+inputType, inputValue);
			
			cTask.setExecutionUser(getUsername());
			customTaskActionOrchestrator.execute(inputMap, cTask, response.getThreadId(), getTeleEnv());
			response.setStatus("SUCCESS");
		}else{
			response.setErrorMessage("This Workaround is not approved");
			response.setStatus("FAILURE");
		}
		
		return response;
	}
	
	@GET
	@Path("/fetchLogsByThreadId")
	public List<TaskLog> fetchLogs(@QueryParam("threadId") String objId) {
		List<TaskLog> taskLogList = null;
		taskLogList = taskLogRepo.findByThreadId(objId);
		return taskLogList;
	}
	
	@GET
	@Path("/fetchBySearchTxt")
	public List<CTask> fetchCTasks(@QueryParam("searchTxt") String searchTxt) {
		List<CTask> cTaskList = null;
		cTaskList = cTaskRepo.findByNameIgnoreCaseLikeOrDescriptionIgnoreCaseLike(searchTxt, searchTxt);
		return cTaskList;
	}

	@GET
	@Path("/fetchByGroup")
	public List<CTask> fetchCTasksByGroup(@QueryParam("groupName") String groupName) {
		List<CTask> cTaskList = null;
		cTaskList = cTaskRepo.findByGroup(groupName);
		return cTaskList;
	}
	
	@GET
	@Path("/getById")
	public CTask getTaskById(@QueryParam("taskId") String taskId) {
		CTask cTask = null;
		cTask = cTaskRepo.findOne(taskId);
		return cTask;
	}

	@POST
	@Path("/approve")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse approveTask(TaskExecutionRequest request) {
		logger.debug("approveTask : " + request);
		ExecResponse response = new ExecResponse();
		CTask cTask = cTaskRepo.findOne(request.getTaskId());
		if(!cTask.isApproved()){
			cTask.setApproved(true);
			cTaskRepo.save(cTask);
			response.setStatus("SUCCESS");
		} else {
			response.setErrorMessage("This Workaround is already approved");
			response.setStatus("FAILURE");
		}
		
		return response;
	}

}
