/**
 * 
 */
package org.zcode.lexec.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.Report;
import org.zcode.lexec.domain.User;
import org.zcode.lexec.engine.CustomTaskActionOrchestrator;
import org.zcode.lexec.engine.SyncTaskOrchestrator;
import org.zcode.lexec.json.C;
import org.zcode.lexec.json.Col;
import org.zcode.lexec.json.ExecResponse;
import org.zcode.lexec.json.ReportJSON;
import org.zcode.lexec.json.Row;
import org.zcode.lexec.repo.CTaskRepo;
import org.zcode.lexec.repo.ReportsRepo;
import org.zcode.lexec.repo.UserRepo;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.sqlx.vo.SQLRow;

import weblogic.auddi.util.Logger;

/**
 * @author fkhan
 *
 */
@Component
@Path("/reports")
@Produces("application/json")
public class ReportsController {

	@Autowired
	ReportsRepo reportsRepo;
	
	@Autowired
	CTaskRepo cTaskRepo;
	
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	SyncTaskOrchestrator syncTaskOrchestrator;
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse saveReport(Report report) {
		ExecResponse response = new ExecResponse();
		reportsRepo.save(report);
		return response;
	}
	
	@GET
	@Path("/fetchAll")
	//@Consumes(MediaType.APPLICATION_JSON)
	public List<Report> fetchAllReports() {
		return reportsRepo.findAll();
	}
	
	@GET
	@Path("/fetch")
	//@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON })
	public ReportJSON fetchReport(@QueryParam("reportId") String reportID) {
		ReportJSON rJson = new ReportJSON();
		Report report = reportsRepo.findOne(reportID);
		rJson.setReportTitle(report.getReportTitle());
		rJson.setReportType(report.getReportType());
		CTask cTask = cTaskRepo.findOne(report.getTaskId());
		ExecResponse response = new ExecResponse();
		
		Map resMap = syncTaskOrchestrator.execute(null, cTask, response.getThreadId(), getTeleEnv());
		System.out.println("ResMap:"+resMap);
		Set<String> paramSet = resMap.keySet();
		for(String param : paramSet){
			System.out.println("Param : " + param);
			System.out.println("Value : " + resMap.get(param));
		}
		SQLExecResponse sqlResponse = (SQLExecResponse)resMap.get(report.getOutParam());
		List<String> colNames = sqlResponse.getColumnNames();
		List<SQLRow> sqlRows = sqlResponse.getSqlRows();
		
		for(String colName : colNames){
			System.out.println("colNAme : "+colName);
			Col col = new Col();
			col.setLabel(colName);
			col.setType("number");
			rJson.addCol(col);
		}
		rJson.getCols().get(0).setLabel("Topping");
		rJson.getCols().get(0).setType("string");
		for(SQLRow sqlRow : sqlRows){
			Row row = new Row();
			System.out.println("sqlRow**");
			List<C> cList = new ArrayList<C>();
			List<String> columnValuesList = sqlRow.getColumnValues();
			for(String columnValue : columnValuesList){
				System.out.println("columnValue : "+columnValue);
				C c = new C();
				c.setV(columnValue);
				cList.add(c);
			}
			row.setC(cList);
			rJson.addRow(row);
		}
		return rJson;
	}
	
	/**
	 * @return
	 */
	private String getTeleEnv() {
		User user = userRepo.findByUsername(getUsername());
		return user.getTeleEnv();
	}
	
	private String getUsername(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.getName();		
	}
	
}
