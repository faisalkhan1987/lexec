/**
 * 
 */
package org.zcode.lexec.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.DSConfig;
import org.zcode.lexec.domain.User;
import org.zcode.lexec.json.ExecResponse;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.repo.UserRepo;
import org.zcode.lexec.sqlx.dao.DSConfigDao;

/**
 * @author fkhan
 *
 */
@Component
@Path("/admin")
@Produces("application/json")
public class AdminController {

	@Autowired
	DSConfigDao dsConfigDao;
	
	@Autowired
	UserRepo userRepo;
	
	@POST
	@Path("/post")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse saveDS(DSConfig dsConfig) {
		ExecResponse response = new ExecResponse();
		dsConfigDao.save(dsConfig);
		return response;
	}
	
	@POST
	@Path("/saveUser")
	@Consumes(MediaType.APPLICATION_JSON)
	public ExecResponse saveUser(User user) {
		ExecResponse response = new ExecResponse();
		userRepo.save(user);
		return response;
	}
}
