/**
 * 
 */
package org.zcode.lexec.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.bpel.BPELConstants;
import org.zcode.lexec.bpel.BpelApiHandlerFactory;
import org.zcode.lexec.engine.TaskActionLogger;
import org.zcode.lexec.json.BpelRequest;
import org.zcode.lexec.json.BpelResponse;
import org.zcode.lexec.json.BpelVariable;

/**
 * @author fkhan
 *
 */
@Component
@Path("/bpelutils")
@Produces(MediaType.APPLICATION_JSON)
public class BpelUtilsController {

	static final Logger logger = Logger.getLogger(BpelUtilsController.class);
	
	@Autowired
	TaskActionLogger taskActionLogger;
		
	@POST
	@Path("/fetchDetails")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse fetchDetails(BpelRequest request) {
		logger.debug("fetchDetails : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		Thread.currentThread().setName(bpelResponse.getThreadId());
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# fetchDetails " + request.toString());
		Map<String, String> bpelVariables = null;
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());		
		try {
			bpelVariables = BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).fetchDetails(request.getFaultId());
//			String auditTrail = null;
//			auditTrail = bpelVariables.get(BPELConstants.AUDIT_TRAIL);
			String faultTrace = null;
			faultTrace = bpelVariables.get(BPELConstants.FAULT_TRACE);
			if(faultTrace != null){
				bpelVariables.remove(BPELConstants.FAULT_TRACE);
			}
//			if(auditTrail != null){
//				bpelVariables.remove(BPELConstants.AUDIT_TRAIL);
//			}
			bpelResponse.setFaultTrace(faultTrace);
//			bpelResponse.setAuditTrail(auditTrail);
			bpelResponse.setBpelVariables(bpelVariables);
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	
	@POST
	@Path("/recoverFault")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse recoverFault(BpelRequest request) {
		logger.debug("recoverFault : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# recoverFault " + request.toString());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());
		
		try {
			BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).recoverBPEL(request.getFaultId());
			//bpelResponse.setBpelVariables(bpelVariables);
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	
	@POST
	@Path("/resubmitFault")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse resubmitFault(BpelRequest request) {
		logger.debug("resubmitFault : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# resubmitFault " + request.toString());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());

		List<BpelVariable> modifiedVariables = request.getModifiedVariables();
		Map<String, String> modifiedVariablesMap = new HashMap<String, String>();
		if(modifiedVariables != null){
			for(BpelVariable variable : modifiedVariables){
				modifiedVariablesMap.put(variable.getName(), variable.getValue());
			}
		}
		
		try {
			BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).resubmitBPEL(request.getFaultId(), modifiedVariablesMap);
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	@POST
	@Path("/continueFault")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse continueFault(BpelRequest request) {
		logger.debug("continueFault : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# continueFault " + request.toString());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());
		
		try {
			BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).continueBPEL(request.getFaultId());
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	
	@POST
	@Path("/abortInstance")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse abortInstance(BpelRequest request) {
		logger.debug("abortInstance : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# abortInstance " + request.toString());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());
		
		try {
			BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).abortBPEL(request.getFaultId());
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	
	@POST
	@Path("/retryFault")
	@Consumes(MediaType.APPLICATION_JSON)
	public BpelResponse retryFault(BpelRequest request) {
		logger.debug("retryFault : " + request);
		String username = getUserName();
		BpelResponse bpelResponse = new BpelResponse();
		taskActionLogger.logOutput(username, "# Starting Execution of " + bpelResponse.getThreadId());
		taskActionLogger.logInfo(username, "# retryFault " + request.toString());
		String providerURL = BPELConstants.PROVIDER_URL;
		providerURL = providerURL.replace("<teleEnv>", request.getEnv());

		List<BpelVariable> modifiedVariables = request.getModifiedVariables();
		Map<String, String> modifiedVariablesMap = new HashMap<String, String>();
		if(modifiedVariables != null){
			for(BpelVariable variable : modifiedVariables){
				modifiedVariablesMap.put(variable.getName(), variable.getValue());
			}
		}
		try {
			BpelApiHandlerFactory.getBPELApiHandler(providerURL, request.getUserName(),
					request.getPassword(), request.getFaultId()).retryBPEL(request.getFaultId(), modifiedVariablesMap);
			bpelResponse.setResponse("SUCCESS");
		} catch (Exception e) {
			logger.error("Exception occurred", e);
			bpelResponse.setErrorMessage(e.getMessage());
			bpelResponse.setResponse("FAILURE");
		} finally{
			taskActionLogger.logOutput(username, "# End");			
		}
		return bpelResponse;
	}
	
	private void validate(BpelRequest request) throws Exception{
		
	}
	
	private String getUserName(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.getName();
	}
	
}
