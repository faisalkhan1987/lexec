package org.zcode.lexec.wsx.vo;

/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
public class SOAPRequest {

    private String xmlRequest;
    
    private String endPointUri;

    /**
     * @return the xmlRequest
     */
    public String getXmlRequest() {
        return xmlRequest;
    }

    /**
     * @param xmlRequest the xmlRequest to set
     */
    public void setXmlRequest(String xmlRequest) {
        this.xmlRequest = xmlRequest;
    }

    /**
     * @return the endPointUri
     */
    public String getEndPointUri() {
        return endPointUri;
    }

    /**
     * @param endPointUri the endPointUri to set
     */
    public void setEndPointUri(String endPointUri) {
        this.endPointUri = endPointUri;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "SoapRequest [xmlRequest=" + xmlRequest + ", endPointUri="
		+ endPointUri + "]";
    }
    
    
}
