package org.zcode.lexec.wsx.vo;

import javax.xml.soap.SOAPMessage;

/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
public class SOAPResponse {

    private SOAPMessage soapMessage;

    /**
     * @return the soapMessage
     */
    public SOAPMessage getSoapMessage() {
        return soapMessage;
    }

    /**
     * @param soapMessage the soapMessage to set
     */
    public void setSoapMessage(SOAPMessage soapMessage) {
        this.soapMessage = soapMessage;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "SoapResponse [soapMessage=" + soapMessage + "]";
    }

    

    
}
