package org.zcode.lexec.wsx.engine;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;

/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
public class SOAPExecutor implements IWSExecutor<SOAPMessage>{

	static final Logger logger = Logger.getLogger(SOAPExecutor.class);
	
    /**
     * @param request
     * @param url
     * @return
     * @throws Exception
     */
    @Override
    public SOAPMessage executeWS(String request, String url) throws Exception {
    	logger.debug("executeWS : " + url);
        SOAPConnection soapConnection = null;
        SOAPMessage soapResponse = null;
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory
                    .newInstance();
            soapConnection = soapConnectionFactory
                    .createConnection();
            SOAPMessage soapRequest = createSOAPRequest(request);
            //soapRequest.writeTo(System.out);
            
            if(logger.isDebugEnabled()){
                StringWriter writer = new StringWriter();  
                
                StreamResult result = new StreamResult(writer);  
                TransformerFactory transformerFactory = TransformerFactory.newInstance();  
                Transformer transformer = transformerFactory.newTransformer();  
                transformer.transform(soapRequest.getSOAPPart().getContent(), result);  
                
                logger.debug(writer.getBuffer());
            }
            //System.out.println(request);
            soapResponse = soapConnection.call(soapRequest,url);

//            if(logger.isDebugEnabled()){
//                StringWriter writer = new StringWriter();  
//                
//                StreamResult result = new StreamResult(writer);  
//                TransformerFactory transformerFactory = TransformerFactory.newInstance();  
//                Transformer transformer = transformerFactory.newTransformer();  
//                transformer.transform(soapResponse.getSOAPPart().getContent(), result);  
//                
//                logger.debug(writer.getBuffer());
//            }
        } finally{
            try {            
                if(soapConnection != null) soapConnection.close();
            } catch (SOAPException ex) {
                //consume the exception
            }
        }
        return soapResponse;
    }
    
    private SOAPMessage createSOAPRequest(String request) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();

        InputStream stream = new ByteArrayInputStream(request.getBytes(StandardCharsets.UTF_8));

        SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), stream);
        soapMessage.getMimeHeaders().addHeader("Auth_user", "selfcare_v2");
        
        return soapMessage;
    }

    
}
