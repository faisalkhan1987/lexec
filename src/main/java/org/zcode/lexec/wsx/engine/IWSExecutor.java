package org.zcode.lexec.wsx.engine;


/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
public interface IWSExecutor<Message> {

    Message executeWS(String request, String url) throws Exception;
}
