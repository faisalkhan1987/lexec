package org.zcode.lexec.wsx.engine;


/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
public class WSExecutionFactory {
    
    private static WSExecutionFactory singletonInstance = null;

    public static final int TYPE_SOAP = 1;
    public static final int TYPE_REST = 2;
    /**
     * @return
     */
    public static final WSExecutionFactory getInstance() {
	if (singletonInstance == null) {
	    singletonInstance = new WSExecutionFactory();
	}
	return singletonInstance;
    }

    /**
     * @param query
     * @return
     */
    public IWSExecutor getExecutor(int type) {
	switch(type){
	case TYPE_SOAP:
	    return new SOAPExecutor();
	}
	return null;
    }
}
