package org.zcode.lexec.wsx.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.zcode.lexec.domain.SOAPTask;


/**
 * @author Faisal_Khan01
 * Sep 22, 2016
 *
 */
public interface SOAPTaskDao extends MongoRepository<SOAPTask, String>{
    
}
