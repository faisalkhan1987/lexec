package org.zcode.lexec.exception;

/**
 * @author Faisal_Khan01
 * Oct 21, 2016
 *
 */
public class PreProcessingException extends Throwable{

    public PreProcessingException(Throwable th){
	super(th);
    }
    
    public PreProcessingException(String message){
	super(message);
    }
}
