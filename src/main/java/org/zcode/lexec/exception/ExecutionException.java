package org.zcode.lexec.exception;

/**
 * @author Faisal_Khan01
 * 
 */
public class ExecutionException extends Throwable {

    public ExecutionException(Throwable e) {
	super(e);
    }

    public ExecutionException(String message){
	super(message);
    }
    /**
	 * 
	 */
    private static final long serialVersionUID = 6724848042633617342L;

}
