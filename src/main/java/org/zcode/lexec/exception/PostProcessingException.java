package org.zcode.lexec.exception;

/**
 * @author Faisal_Khan01
 * Oct 21, 2016
 *
 */
public class PostProcessingException extends Throwable{

    public PostProcessingException(Throwable th){
	super(th);
    }
    
    public PostProcessingException(String message){
	super(message);
    }
}
