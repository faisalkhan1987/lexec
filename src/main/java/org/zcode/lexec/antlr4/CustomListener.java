package org.zcode.lexec.antlr4;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarBaseListener;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarParser;
import org.zcode.lexec.common.ParamResolver;

/**
 * 
 */

/**
 * @author fkhan
 *
 */
@Component
public class CustomListener extends RuleSetGrammarBaseListener{

	@Autowired
	ParamResolver paramResolver;
	
	private final Stack<String> argStack = new Stack<String>();
	private final Stack<Integer> opStack = new Stack<Integer>();
	
	boolean result = false;
	
	public boolean getResult(){
		return result;
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(@NotNull TerminalNode node) { 
		//System.out.println("visitTerminal - " + node.getText() + " Node Type : " + node.getSymbol().getType());
		final Token symbol = node.getSymbol();
        final int type = symbol.getType();
        switch (type) {
        case RuleSetGrammarParser.IDENTIFIER:
        	this.argStack.push(paramResolver.resolveParamValue(symbol.getText()));
        	break;
        case RuleSetGrammarParser.DECIMAL:
            this.argStack.push(symbol.getText());
            break;
        case RuleSetGrammarParser.MULT:
        case RuleSetGrammarParser.DIV:
        case RuleSetGrammarParser.PLUS:
        case RuleSetGrammarParser.MINUS:
        case RuleSetGrammarParser.EQ:
        case RuleSetGrammarParser.GT:
        case RuleSetGrammarParser.GE:
        case RuleSetGrammarParser.LT:
        case RuleSetGrammarParser.LE:
            this.opStack.add(type);
            break;
        default:
            break;
        }
	}
	
	@Override public void enterComparisonExpression(@NotNull RuleSetGrammarParser.ComparisonExpressionContext ctx) { 
		//System.out.println("enterComparisonExpression : " + ctx.getText());
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitComparisonExpression(@NotNull RuleSetGrammarParser.ComparisonExpressionContext ctx) {
		final String right = this.argStack.pop();
        final String left = this.argStack.pop();
        final int op = this.opStack.pop();
        Double rightDouble;
        Double leftDouble;
        switch(op){
        case RuleSetGrammarParser.EQ:
        	this.argStack.push(new Boolean(right.equals(left)).toString());
        	break;
        case RuleSetGrammarParser.GT:
        	rightDouble = Double.parseDouble(right);
        	leftDouble = Double.parseDouble(left);
        	this.argStack.push(new Boolean(leftDouble > rightDouble).toString());
        	break;
        case RuleSetGrammarParser.GE:
        	rightDouble = Double.parseDouble(right);
        	leftDouble = Double.parseDouble(left);
        	this.argStack.push(new Boolean(leftDouble >= rightDouble).toString());
        	break;
        case RuleSetGrammarParser.LT:
        	rightDouble = Double.parseDouble(right);
        	leftDouble = Double.parseDouble(left);
        	this.argStack.push(new Boolean(leftDouble < rightDouble).toString());
        	break;
        case RuleSetGrammarParser.LE:
        	rightDouble = Double.parseDouble(right);
        	leftDouble = Double.parseDouble(left);
        	this.argStack.push(new Boolean(leftDouble <= rightDouble).toString());
        	break;
        }
		System.out.println("exitComparisonExpression : "+ctx.getText());
	}
	
	@Override public void exitLogicalExpressionAnd(@NotNull RuleSetGrammarParser.LogicalExpressionAndContext ctx) { 
		System.out.println("exitLogicalExpressionAnd : "+ ctx.getText());
		boolean right = new Boolean(this.argStack.pop());
		boolean left = new Boolean(this.argStack.pop());
		System.out.println("Right : " + right + " Left : " + left);
		this.argStack.push(new Boolean(right && left).toString());
        
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitLogicalExpressionOr(@NotNull RuleSetGrammarParser.LogicalExpressionOrContext ctx) { 
		System.out.println("exitLogicalExpressionOr : "+ ctx.getText());
		boolean right = new Boolean(this.argStack.pop());
		boolean left = new Boolean(this.argStack.pop());
		System.out.println("Right : " + right + " Left : " + left);
		this.argStack.push(new Boolean(right || left).toString());
	}
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitSingle_rule(@NotNull RuleSetGrammarParser.Single_ruleContext ctx) { 
		System.out.println("exitSingle_rule");
		result = new Boolean(this.argStack.pop());
	}
}
