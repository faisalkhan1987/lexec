package org.zcode.lexec.domain;

import java.sql.Timestamp;

import org.springframework.data.mongodb.core.index.Indexed;

/**
 * @author faisal_khan01
 * 
 */
//@Entity
//@Table(name = "LEXEC_SQL_DS_CONFIG")
public class DSConfig {

//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQ")
//    @SequenceGenerator(name = "ID_SEQ", sequenceName = "DS_CONFIG_ID_SEQ")
//    @Column(name = "DS_CONFIG_ID", unique = true, nullable = false)
//    private Long dsConfigID;

//    @Column(name = "DS_NAME")
    @Indexed(unique = true)
    private String dsName;

//    @Column(name = "CTX_PATH")
    private String contextPath;

//    @Column(name = "DRIVER_CLS_NAME")
    private String driverClassName;

//    @Column(name = "USER_NAME")
    private String userName;

//    @Column(name = "ENC_PASSWORD")
    private String encryptedPassword;

//    @Column(name = "JDBC_CON_STRING")
    private String jdbcConnectionString;

//    @Column(name = "CREATION_TIME")
    private Timestamp creationTime;

//    @Column(name = "CREATION_USER")
    private String creationUser;

//    @Column(name = "MODIFICATION_TIME")
    private Timestamp modificationTime;

//    @Column(name = "MODIFICATION_USER")
    private String modificationUser;


    /**
     * @return the dsName
     */
    public String getDsName() {
	return dsName;
    }

    /**
     * @param dsName
     *            the dsName to set
     */
    public void setDsName(String dsName) {
	this.dsName = dsName;
    }

    /**
     * @return the contextPath
     */
    public String getContextPath() {
	return contextPath;
    }

    /**
     * @param contextPath
     *            the contextPath to set
     */
    public void setContextPath(String contextPath) {
	this.contextPath = contextPath;
    }

    /**
     * @return the driverClassName
     */
    public String getDriverClassName() {
	return driverClassName;
    }

    /**
     * @param driverClassName
     *            the driverClassName to set
     */
    public void setDriverClassName(String driverClassName) {
	this.driverClassName = driverClassName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

    /**
     * @return the encryptedPassword
     */
    public String getEncryptedPassword() {
	return encryptedPassword;
    }

    /**
     * @param encryptedPassword
     *            the encryptedPassword to set
     */
    public void setEncryptedPassword(String encryptedPassword) {
	this.encryptedPassword = encryptedPassword;
    }

    /**
     * @return the creationTime
     */
    public Timestamp getCreationTime() {
	return creationTime;
    }

    /**
     * @param creationTime
     *            the creationTime to set
     */
    public void setCreationTime(Timestamp creationTime) {
	this.creationTime = creationTime;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
	return creationUser;
    }

    /**
     * @param creationUser
     *            the creationUser to set
     */
    public void setCreationUser(String creationUser) {
	this.creationUser = creationUser;
    }

    /**
     * @return the modificationTime
     */
    public Timestamp getModificationTime() {
	return modificationTime;
    }

    /**
     * @param modificationTime
     *            the modificationTime to set
     */
    public void setModificationTime(Timestamp modificationTime) {
	this.modificationTime = modificationTime;
    }

    /**
     * @return the modificationUser
     */
    public String getModificationUser() {
	return modificationUser;
    }

    /**
     * @param modificationUser
     *            the modificationUser to set
     */
    public void setModificationUser(String modificationUser) {
	this.modificationUser = modificationUser;
    }

    /**
     * @return the jdbcConnectionString
     */
    public String getJdbcConnectionString() {
        return jdbcConnectionString;
    }

    /**
     * @param jdbcConnectionString the jdbcConnectionString to set
     */
    public void setJdbcConnectionString(String jdbcConnectionString) {
        this.jdbcConnectionString = jdbcConnectionString;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "DSConfig [dsName=" + dsName
		+ ", contextPath=" + contextPath + ", driverClassName="
		+ driverClassName + ", userName=" + userName
		+ ", encryptedPassword=" + encryptedPassword
		+ ", jdbcConnectionString=" + jdbcConnectionString + "]";
    }

}
