package org.zcode.lexec.domain;

/**
 * @author faisal_khan01
 * 
 */
//@Entity
//@Table(name = "LEXEC_SQL_TASK")
//@PrimaryKeyJoinColumn(name="TASK_ID")
public class SQLTask extends Task {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

//    @Lob
//    @Column(name = "QUERY_TXT")
    private String queryText;

//    @ManyToOne
//    @JoinColumn(name = "DS_CONFIG_ID")
//    private DSConfig dsConfig;
    private String dbName;

    public SQLTask(){
    	super();
    	this.sleepTime = 200;
    }
    /**
     * @return the queryText
     */
    public String getQueryText() {
    	return queryText;
    }

    /**
     * @param queryText
     *            the queryText to set
     */
    public void setQueryText(String queryText) {
	this.queryText = queryText;
    }


    /**
     * @return the dbName
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * @param dbName the dbName to set
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "SQLTask [queryText=" + queryText + ", dbName=" + dbName
		+ "]";
    }



}
