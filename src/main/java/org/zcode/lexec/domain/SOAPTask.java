package org.zcode.lexec.domain;

import java.sql.Timestamp;

/**
 * @author Faisal_Khan01
 *
 */
//@Entity
//@Table(name = "LEXEC_WS_TASK")
//@PrimaryKeyJoinColumn(name="TASK_ID")  
public class SOAPTask extends Task{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

//    @Lob
//    @Column(name = "WS_REQUEST")
    private String wsRequest;

//    @Column(name = "WS_ENDPOINT")
    private String wsEndpoint;

    public SOAPTask(){
    	super();
    	this.sleepTime = 3000;
    }
    /**
     * @return the wsRequest
     */
    public String getWsRequest() {
        return wsRequest;
    }

    /**
     * @param wsRequest the wsRequest to set
     */
    public void setWsRequest(String wsRequest) {
        this.wsRequest = wsRequest;
    }

    /**
     * @return the wsEndpoint
     */
    public String getWsEndpoint() {
        return wsEndpoint;
    }

    /**
     * @param wsEndpoint the wsEndpoint to set
     */
    public void setWsEndpoint(String wsEndpoint) {
        this.wsEndpoint = wsEndpoint;
    }

    /**
     * @return the creationTime
     */
    public Timestamp getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * @param creationUser the creationUser to set
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * @return the modificationTime
     */
    public Timestamp getModificationTime() {
        return modificationTime;
    }

    /**
     * @param modificationTime the modificationTime to set
     */
    public void setModificationTime(Timestamp modificationTime) {
        this.modificationTime = modificationTime;
    }

    /**
     * @return the modificationUser
     */
    public String getModificationUser() {
        return modificationUser;
    }

    /**
     * @param modificationUser the modificationUser to set
     */
    public void setModificationUser(String modificationUser) {
        this.modificationUser = modificationUser;
    }

//    /* (non-Javadoc)
//     * @see java.lang.Object#toString()
//     */
//    @Override
//    public String toString() {
//	return "WSTask [wsRequest=" + wsRequest + ", wsEndpoint=" + wsEndpoint
//		+ "]";
//    }

      
}
