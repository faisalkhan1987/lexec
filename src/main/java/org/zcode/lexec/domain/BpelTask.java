/**
 * 
 */
package org.zcode.lexec.domain;

/**
 * @author fkhan
 *
 */
public class BpelTask extends Task{
	
	private String instanceVariable;
	
	private BpelAction action;
	
	private String bpelVariableName;
	
	private String sourceString;
	
	private String targetString;
	
	/**
	 * Should be always xml tag like - <SimSwapInfo> or </OrderInfo>
	 */
	private String insertAfterNode;
	
	/**
	 * value should be like <ns41:OldSimNumber>#SIM_NUMBER</ns41:OldSimNumber>
	 */
	private String newNode;
	
	public enum BpelAction {
		RESUBMIT, CONTINUE, ABORT, RECOVER, REPLACE_VARIABLE, RETRY, FETCH_FAULT_DETAILS, REPLACE_VARIABLE_INSERT
	}

	/**
	 * @return the insertAfterNode
	 */
	public String getInsertAfterNode() {
		return insertAfterNode;
	}

	/**
	 * @param insertAfterNode the insertAfterNode to set
	 */
	public void setInsertAfterNode(String insertAfterNode) {
		this.insertAfterNode = insertAfterNode;
	}

	/**
	 * @return the newNode
	 * 
	 */
	public String getNewNode() {
		return newNode;
	}

	/**
	 * @param newNode the newNode to set
	 */
	public void setNewNode(String newNode) {
		this.newNode = newNode;
	}

	/**
	 * @return the action
	 */
	public BpelAction getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(BpelAction action) {
		this.action = action;
	}

	/**
	 * @return the instanceVariable
	 */
	public String getInstanceVariable() {
		return instanceVariable;
	}

	/**
	 * @param instanceVariable the instanceVariable to set
	 */
	public void setInstanceVariable(String instanceVariable) {
		this.instanceVariable = instanceVariable;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BpelTask [instanceVariable=" + instanceVariable + ", action=" + action + "]";
	}

	/**
	 * @return the bpelVariableName
	 */
	public String getBpelVariableName() {
		return bpelVariableName;
	}

	/**
	 * @param bpelVariableName the bpelVariableName to set
	 */
	public void setBpelVariableName(String bpelVariableName) {
		this.bpelVariableName = bpelVariableName;
	}

	/**
	 * @return the sourceString
	 */
	public String getSourceString() {
		return sourceString;
	}

	/**
	 * @param sourceString the sourceString to set
	 */
	public void setSourceString(String sourceString) {
		this.sourceString = sourceString;
	}

	/**
	 * @return the targetString
	 */
	public String getTargetString() {
		return targetString;
	}

	/**
	 * @param targetString the targetString to set
	 */
	public void setTargetString(String targetString) {
		this.targetString = targetString;
	}

	

	
	
	
}
