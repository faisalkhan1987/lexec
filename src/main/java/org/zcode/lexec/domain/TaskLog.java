package org.zcode.lexec.domain;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.zcode.lexec.constants.TaskLogConstants;
import org.zcode.lexec.json.BpelRequest;
import org.zcode.lexec.json.BpelResponse;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.wsx.vo.SOAPRequest;
import org.zcode.lexec.wsx.vo.SOAPResponse;

/**
 * @author Faisal_Khan01
 * Oct 20, 2016
 *
 */
public class TaskLog {

	public TaskLog(){
		this.threadId = Thread.currentThread().getName();
		this.createdDate = new Date();
	}
	public TaskLog(String user, String log){
		this();
		this.user = user;
		this.log = log;
    }
    
	public TaskLog(String user, String log, String type){
		this(user, log);
		this.type = type;
	}
	
	public TaskLog(SQLExecRequest sqlInput){
		this();
		this.sqlInput = sqlInput;
		this.type = TaskLogConstants.SQL_INPUT;
	}
	
	public TaskLog(SQLExecResponse sqlOutput){
		this();
		this.sqlOutput = sqlOutput;
		this.type = TaskLogConstants.SQL_OUTPUT;
	}
	
	public TaskLog(SOAPRequest soapInput){
		this();
		this.soapInput = soapInput;
		this.type = TaskLogConstants.WS_INPUT;
	}
	public TaskLog(BpelRequest bpelRequest){
		this();
		this.bpelRequest = bpelRequest;
		this.type = TaskLogConstants.BPEL_INPUT;
	}
	public TaskLog(SOAPResponse soapOutput){
		this();
		this.soapOutput = soapOutput;
		this.type = TaskLogConstants.WS_OUTPUT;
	}
    private String threadId;
    
    @DateTimeFormat(iso = ISO.DATE_TIME)
	private Date createdDate;
    
    private String log;

    private String type = TaskLogConstants.INFO;
    
    private SQLExecRequest sqlInput;
    private SQLExecResponse sqlOutput;
    private BpelRequest bpelRequest;
    
    private SOAPRequest soapInput;
    private SOAPResponse soapOutput;
    
    private String user;
    
    /**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
     * @return the threadId
     */
    public String getThreadId() {
        return threadId;
    }

    /**
     * @param threadId the threadId to set
     */
    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    /**
     * @return the log
     */
    public String getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	/**
	 * @return the sqlInput
	 */
	public SQLExecRequest getSqlInput() {
		return sqlInput;
	}
	/**
	 * @param sqlInput the sqlInput to set
	 */
	public void setSqlInput(SQLExecRequest sqlInput) {
		this.sqlInput = sqlInput;
	}
	/**
	 * @return the sqlOutput
	 */
	public SQLExecResponse getSqlOutput() {
		return sqlOutput;
	}
	/**
	 * @param sqlOutput the sqlOutput to set
	 */
	public void setSqlOutput(SQLExecResponse sqlOutput) {
		this.sqlOutput = sqlOutput;
	}
	/**
	 * @return the soapInput
	 */
	public SOAPRequest getSoapInput() {
		return soapInput;
	}
	/**
	 * @param soapInput the soapInput to set
	 */
	public void setSoapInput(SOAPRequest soapInput) {
		this.soapInput = soapInput;
	}
	/**
	 * @return the soapOutput
	 */
	public SOAPResponse getSoapOutput() {
		return soapOutput;
	}
	/**
	 * @param soapOutput the soapOutput to set
	 */
	public void setSoapOutput(SOAPResponse soapOutput) {
		this.soapOutput = soapOutput;
	}
	
	
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the bpelRequest
	 */
	public BpelRequest getBpelRequest() {
		return bpelRequest;
	}
	/**
	 * @param bpelRequest the bpelRequest to set
	 */
	public void setBpelRequest(BpelRequest bpelRequest) {
		this.bpelRequest = bpelRequest;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TaskLog [threadId=" + threadId + ", log=" + log + ", type=" + type + "]";
	}
    
    
}
