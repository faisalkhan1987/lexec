/**
 * 
 */
package org.zcode.lexec.domain;

import org.springframework.data.annotation.Id;

/**
 * @author fkhan
 *
 */
public class Report {

	@Id
    protected String reportId;

	private String reportName;
	
	private String reportTitle;

	private String reportType;//bar-chart, pie-chart, etc
	
	private String taskId;
	
	private String outParam;
	
	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the taskId
	 */
	public String getTaskId() {
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	/**
	 * @return the outParam
	 */
	public String getOutParam() {
		return outParam;
	}

	/**
	 * @param outParam the outParam to set
	 */
	public void setOutParam(String outParam) {
		this.outParam = outParam;
	}
	
	/**
	 * @return the reportTitle
	 */
	public String getReportTitle() {
		return reportTitle;
	}

	/**
	 * @param reportTitle the reportTitle to set
	 */
	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
	
	
	
}
