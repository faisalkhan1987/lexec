/**
 * 
 */
package org.zcode.lexec.domain;

/**
 * @author fkhan
 *
 */
public class User {

	private String username;
	
	private String password;
	
	private String[] roles;

	private String teleEnv;
	
	private String[] bpelEnvs;
	
	public User(){}
	
	public User(String username, String password){
		this.username = username;
		this.password = password;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the role
	 */
	public String[] getRoles() {
		return roles;
	}

	/**
	 * @param role the role to set
	 */
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	/**
	 * @return the teleEnv
	 */
	public String getTeleEnv() {
		return teleEnv;
	}
	/**
	 * @param teleEnv the teleEnv to set
	 */
	public void setTeleEnv(String teleEnv) {
		this.teleEnv = teleEnv;
	}
	/**
	 * @return the bpelEnvs
	 */
	public String[] getBpelEnvs() {
		return bpelEnvs;
	}
	/**
	 * @param bpelEnvs the bpelEnvs to set
	 */
	public void setBpelEnvs(String[] bpelEnvs) {
		this.bpelEnvs = bpelEnvs;
	}
	
	
}
