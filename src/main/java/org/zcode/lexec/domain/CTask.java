package org.zcode.lexec.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Faisal_Khan01 Sep 23, 2016
 * 
 */
//@Entity
//@Table(name = "LEXEC_C_TASK")
//@PrimaryKeyJoinColumn(name = "TASK_ID")
public class CTask extends Task {

//    @ManyToMany(cascade = { CascadeType.ALL }, fetch=FetchType.EAGER)
//    @JoinTable(name = "LEXEC_CTASK_MAPPING", 
//    	joinColumns = { @JoinColumn(name = "TASK_ID") }, inverseJoinColumns = { @JoinColumn(name = "CHILD_TASK_ID") })
    private List<Task> childTasks = new ArrayList<Task>();

    private String expression;
    
    private String group;
    
    private String iterationCount;
    
    private boolean approved = true;
    
    private boolean approvedForBulkExec = true;
    
    /**
     * @return the childTasks
     */
    public List<Task> getChildTasks() {
	if(childTasks == null){
	    childTasks = new ArrayList<Task>();
	}
        return childTasks;
    }

    /**
     * @param childTasks the childTasks to set
     */
    public void setChildTasks(List<Task> childTasks) {
        this.childTasks = childTasks;
    }

	/**
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * @param expression the expression to set
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * @return the iterationCount
	 */
	public String getIterationCount() {
		return iterationCount;
	}

	/**
	 * @param iterationCount the iterationCount to set
	 */
	public void setIterationCount(String iterationCount) {
		this.iterationCount = iterationCount;
	}

	/**
	 * @return the approved
	 */
	public boolean isApproved() {
		return approved;
	}

	/**
	 * @param approved the approved to set
	 */
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	/**
	 * @return the approvedForBulkExec
	 */
	public boolean isApprovedForBulkExec() {
		return approvedForBulkExec;
	}

	/**
	 * @param approvedForBulkExec the approvedForBulkExec to set
	 */
	public void setApprovedForBulkExec(boolean approvedForBulkExec) {
		this.approvedForBulkExec = approvedForBulkExec;
	}

	public List<String> getOutParams(){
		List<String> outParams = new ArrayList<String>();
		for(Task task : childTasks){
			outParams.add(task.getOutParam());
		}
		return outParams;
	}
}
