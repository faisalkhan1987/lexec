/**
 * 
 */
package org.zcode.lexec.domain;

/**
 * @author fkhan
 *
 */
public class DataAnalysisConfig {

	private String inputType;
	
	private String strategyName;

	/**
	 * @return the inputType
	 */
	public String getInputType() {
		return inputType;
	}

	/**
	 * @param inputType the inputType to set
	 */
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	/**
	 * @return the strategyName
	 */
	public String getStrategyName() {
		return strategyName;
	}

	/**
	 * @param strategyName the strategyName to set
	 */
	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}
	
	
}
