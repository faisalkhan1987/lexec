/**
 * 
 */
package org.zcode.lexec.domain;

import java.util.List;

/**
 * @author fkhan
 *
 */
public class Config {

	private List<String> groups;

	/**
	 * @return the groups
	 */
	public List<String> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	
	
}
