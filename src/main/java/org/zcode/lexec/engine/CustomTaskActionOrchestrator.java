package org.zcode.lexec.engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.factory.TaskHandlerFactory;

/**
 * @author Faisal_Khan01 Sep 29, 2016
 * 
 */
@Component
public class CustomTaskActionOrchestrator implements ITaskActionOrchestrator {

	static final Logger logger = Logger.getLogger(CustomTaskActionOrchestrator.class);
	
	@Autowired
	TaskHandlerFactory taskHandlerFactory;

	@Autowired
	TaskActionLogger taskActionLogger;
	
	@Autowired
	SyncTaskOrchestrator syncTaskOrchestrator;

	@Override
	public Map execute( Map inputMap, Task task, String threadId, String teleEnv) {
		
		return syncTaskOrchestrator.execute(inputMap, task, threadId, teleEnv);
	}

	/* (non-Javadoc)
	 * @see org.zcode.lexec.engine.ITaskActionOrchestrator#bulkExecute(java.util.Map, org.zcode.lexec.domain.Task, java.lang.String, java.lang.String)
	 */
	@Override
	public void bulkExecute(Map inputMap, Task task, String threadId, String teleEnv) {
		Thread.currentThread().setName(threadId);

		try {
			SymbolTable.getInstance().init();

			SymbolTable.getInstance().putAll(inputMap);

			logger.debug("taskHandlerFactory : " + taskHandlerFactory);
			
			taskHandlerFactory.processTask(task, teleEnv);

		} catch (PreProcessingException e) {
			taskActionLogger.logException("# PreProcessingException : "+e.getMessage());
			logger.error("PreProcessingException", e);
		} catch (ExecutionException e) {
			taskActionLogger.logException("# ExecutionException : "+e.getMessage());
			logger.error("ExecutionException", e);
		} catch (Exception e) {
			taskActionLogger.logException("# ExecutionException : "+e.getMessage());
			logger.error("Exception", e);
		} finally {
			SymbolTable.getInstance().close();
//			taskActionLogger.logOutput(task.getExecutionUser(), "# End");
		}		
	}

	/* (non-Javadoc)
	 * @see org.zcode.lexec.engine.ITaskActionOrchestrator#bulkAsyncExecute(java.lang.String[], org.zcode.lexec.domain.CTask, java.lang.String, long)
	 */
	@Override
	public void bulkAsyncExecute(String[] inputList, CTask cTask, String threadID, 
			long sleepTime, String teleEnv, String username) {
		taskActionLogger.logOutput(cTask.getExecutionUser(), "# Starting Bulk Execution of "+ cTask.toString() + " - ThreadID : " + threadID);
		for(String input : inputList){
			String[] inputValues = input.split(";");

			List<String> inputParams = cTask.getInputParams();
			Map<String, String> inputMap = new HashMap<String, String>();
			
			int index = 0;
			//inputParams.forEach((inputParam) -> inputMap.put(inputParam, inputValues[i]));

			for (String inputParam : inputParams) {
				// Put the param values from request into the inputMap
//				System.out.print(inputParam + " :" + inputValues[index] + ", ");
				inputMap.put(inputParam, inputValues[index]);
				index++;
			}
			this.bulkExecute(inputMap, cTask, threadID, teleEnv);
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println("******");
		}
		taskActionLogger.logOutput(username, "# End");
	}

//	private String getUserName(){
//		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//		return authentication.getName();
//	}
	
}
