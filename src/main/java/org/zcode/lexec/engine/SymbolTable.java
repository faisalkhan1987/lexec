/**
 * 
 */
package org.zcode.lexec.engine;

import java.util.HashMap;
import java.util.Map;

/**
 * @author faisal_khan01
 * Oct 5, 2016
 *
 */
public class SymbolTable {

    private final ThreadLocal thLocal = new ThreadLocal();
    
    private final ThreadLocal thLocalBpel = new ThreadLocal();
    
    private final ThreadLocal thLocalBpelModifiedVariables = new ThreadLocal();

    private static SymbolTable singletonInstance = null;
    
    private SymbolTable(){}
    
    /**
     * @return
     */
    public static final SymbolTable getInstance(){
		if(singletonInstance == null){
		    singletonInstance = new SymbolTable();
		}
		return singletonInstance;
    }
    
    public final void init(){
		Map valueMap = new HashMap();
		thLocal.set(valueMap);
		Map bpelValueMap = new HashMap();
		thLocalBpel.set(bpelValueMap);
		Map bpelModifiedMap = new HashMap();
		thLocalBpelModifiedVariables.set(bpelModifiedMap);
    }
    
    public final void close(){
		Map valueMap = (Map)thLocal.get();
		valueMap.clear();
		valueMap = null;
		
		valueMap = (Map)thLocalBpel.get();
		valueMap.clear();
		valueMap = null;
		
		valueMap = (Map)thLocalBpelModifiedVariables.get();
		valueMap.clear();
		valueMap = null;
    }
    
    public void put(String key, Object value){
    	((Map)thLocal.get()).put(key, value);
    }
    
    public void putAll(Map map){
    	if(map != null)
    		((Map)thLocal.get()).putAll(map);
    }
    
    public Map getAll(){
    	return (Map)thLocal.get();
    }
    
    public Object get(String key){
    	return ((Map)thLocal.get()).get(key);
    }
    
    public void putBpelVariable(String key, Object value){
    	((Map)thLocalBpel.get()).put(key, value);
    }
    
    public void putModifiedBpelVariable(String key, Object value){
    	((Map)thLocalBpelModifiedVariables.get()).put(key, value);
    }
    
    public void putAllBpelVariables(Map map){
    	((Map)thLocalBpel.get()).putAll(map);
    }
    public Object getBpelVariable(String key){
    	return ((Map)thLocalBpel.get()).get(key);
    }
    
    public Map getAllBpelVariables(){
    	return (Map)thLocalBpel.get();
    }
    
    public Map getModifiedBpelVariables(){
    	return (Map)thLocalBpelModifiedVariables.get();
    }
    
}
