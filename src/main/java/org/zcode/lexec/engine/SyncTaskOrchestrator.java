/**
 * 
 */
package org.zcode.lexec.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.factory.TaskHandlerFactory;

/**
 * @author fkhan
 *
 */
@Component
public class SyncTaskOrchestrator {

	static final Logger logger = Logger.getLogger(SyncTaskOrchestrator.class);
	
	@Autowired
	TaskHandlerFactory taskHandlerFactory;

	@Autowired
	TaskActionLogger taskActionLogger;
			
	public Map execute( Map inputMap, Task task, String threadId, String teleEnv) {
		Thread.currentThread().setName(threadId);
		taskActionLogger.logOutput(task.getExecutionUser(), "# Starting Execution of "+ task.toString() + " - ThreadID : " + threadId);

		try {
			SymbolTable.getInstance().init();

			SymbolTable.getInstance().putAll(inputMap);

			logger.debug("taskHandlerFactory : " + taskHandlerFactory);
			
			taskHandlerFactory.processTask(task, teleEnv);
			Map map = SymbolTable.getInstance().getAll();
			Map returnMap = new HashMap();
			Set<String> keySet = map.keySet();
			for(String key : keySet){
				System.out.println("Key : "+key+" | Value : "+map.get(key));
				returnMap.put(key, map.get(key));
			}
			
			return returnMap;
		} catch (PreProcessingException e) {
			taskActionLogger.logException("# PreProcessingException : "+e.getMessage());
			logger.error("PreProcessingException", e);
		} catch (ExecutionException e) {
			taskActionLogger.logException("# ExecutionException : "+e.getMessage());
			logger.error("ExecutionException", e);
		} catch (Exception e) {
			taskActionLogger.logException("# ExecutionException : "+e.getMessage());
			logger.error("Exception", e);
		} finally {
			SymbolTable.getInstance().close();
			taskActionLogger.logOutput(task.getExecutionUser(), "# End");
		}
		return null;

	}
}
