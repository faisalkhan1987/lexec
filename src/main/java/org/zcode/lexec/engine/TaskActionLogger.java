package org.zcode.lexec.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.constants.TaskLogConstants;
import org.zcode.lexec.domain.TaskLog;
import org.zcode.lexec.json.BpelRequest;
import org.zcode.lexec.repo.TaskLogRepo;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.wsx.vo.SOAPRequest;
import org.zcode.lexec.wsx.vo.SOAPResponse;

/**
 * @author Faisal_Khan01
 * Oct 20, 2016
 *
 */
@Component
public class TaskActionLogger {

    @Autowired
    TaskLogRepo taskLogRepo;
    
	public void logOutput(String user, String details){	
		taskLogRepo.save(new TaskLog(user, details, TaskLogConstants.OUTPUT));
    }

	public void logInfo(String user, String details){	
		taskLogRepo.save(new TaskLog(user, details));
    }
	
	public void logAlert(String user, String message){
		taskLogRepo.save(new TaskLog(user, message, TaskLogConstants.ALERT));
	}
	public void logException(String details){
		taskLogRepo.save(new TaskLog(details, TaskLogConstants.EXCEPTION));
	}

	public void logInput(BpelRequest bpelRequest){
		taskLogRepo.save(new TaskLog(bpelRequest));
	}
	
	public void logInput(SOAPRequest soapInput){
		taskLogRepo.save(new TaskLog(soapInput));
	}
	
	public void logOutput(SOAPResponse soapResponse){
		taskLogRepo.save(new TaskLog(soapResponse));
	}
	
	public void logInput(SQLExecRequest sqlInput){
		taskLogRepo.save(new TaskLog(sqlInput));
	}
	public void logOutput(SQLExecResponse sqlOutput){
		taskLogRepo.save(new TaskLog(sqlOutput));
	}
	
	public void logError(String error){
		taskLogRepo.save(new TaskLog(error, TaskLogConstants.ERROR));
	}
}
