package org.zcode.lexec.engine;

import java.util.Map;

import org.springframework.scheduling.annotation.Async;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;

/**
 * @author Faisal_Khan01
 * Sep 28, 2016
 *
 */
public interface ITaskActionOrchestrator {

    @Async
    Map execute(Map inputMap, Task task, String threadId, String teleEnv) ;
    
    void bulkExecute(Map inputMap, Task task, String threadId, String teleEnv) ;
    
    @Async
    void bulkAsyncExecute(String[] inputList, CTask cTask, String threadID, long sleepTime, String teleEnv, String username);
}
