/**
 * 
 */
package org.zcode.lexec.json;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fkhan
 *
 */
public class Row {

	private List<C> c;
	
	


	public void addC(C c){
		if(this.c == null){
			this.c=new ArrayList<C>();
		}
		this.c.add(c);
	}




	/**
	 * @return the c
	 */
	public List<C> getC() {
		return c;
	}




	/**
	 * @param c the c to set
	 */
	public void setC(List<C> c) {
		this.c = c;
	}
	
}

