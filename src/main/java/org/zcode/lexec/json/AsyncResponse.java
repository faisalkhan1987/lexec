/**
 * 
 */
package org.zcode.lexec.json;

import org.bson.types.ObjectId;

/**
 * @author fkhan
 *
 */
public class AsyncResponse {

	protected String threadId;

	public AsyncResponse(){
		// Creating a new ObjectId which will be used as a reference for the logger
		ObjectId objId = new ObjectId();
		Thread.currentThread().setName(objId.toString());
		this.setThreadId(objId.toString());
	}
	/**
	 * @return the threadId
	 */
	public String getThreadId() {
		return threadId;
	}

	/**
	 * @param threadId the threadId to set
	 */
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}
	
	
}
