package org.zcode.lexec.json;

import java.util.List;

/**
 * @author Faisal_Khan01 Oct 13, 2016
 * 
 */
public class Strategy {

    private String name;
    
    private String description;
    
    private String group;
    
    private List<String> inputParams;
    
    private List<String> passwordParams;
    
    private List<SubTask> tasks;

    /**
	 * @return the passwordParams
	 */
	public List<String> getPasswordParams() {
		return passwordParams;
	}

	/**
	 * @param passwordParams the passwordParams to set
	 */
	public void setPasswordParams(List<String> passwordParams) {
		this.passwordParams = passwordParams;
	}

	/**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the inputParams
     */
    public List<String> getInputParams() {
        return inputParams;
    }

    /**
     * @param inputParams the inputParams to set
     */
    public void setInputParams(List<String> inputParams) {
        this.inputParams = inputParams;
    }

    /**
     * @return the tasks
     */
    public List<SubTask> getTasks() {
        return tasks;
    }

    /**
     * @param tasks the tasks to set
     */
    public void setTasks(List<SubTask> tasks) {
        this.tasks = tasks;
    }

    /**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Strategy [name=" + name + ", description=" + description
		+ ", inputParams=" + inputParams + ", tasks=" + tasks + "]";
    }

    
    
}
