package org.zcode.lexec.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Faisal_Khan01
 * Oct 19, 2016
 *
 */
public class TaskExecutionRequest {

    private String taskId;
    
    private List<String> inputParams;

    private List<String> passwordParams;
    
    /**
     * @return the taskId
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    
    /**
     * @return the inputParams
     */
    public List<String> getInputParams() {
    	if(inputParams == null){
    		inputParams = new ArrayList<String>();
    	}
        return inputParams;
    }

    /**
     * @param inputParams the inputParams to set
     */
    public void setInputParams(List<String> inputParams) {
        this.inputParams = inputParams;
    }

    
    /**
	 * @return the passwordParams
	 */
	public List<String> getPasswordParams() {
		if(passwordParams == null){
			passwordParams = new ArrayList<String>();
    	}
		return passwordParams;
	}

	/**
	 * @param passwordParams the passwordParams to set
	 */
	public void setPasswordParams(List<String> passwordParams) {
		this.passwordParams = passwordParams;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "TaskExecutionRequest [taskId=" + taskId + ", inputParams="
		+ getInputParams().size() + "]";
    }
    
    
}
