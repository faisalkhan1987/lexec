/**
 * 
 */
package org.zcode.lexec.json;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fkhan
 *
 */
public class ReportJSON {

	private String reportTitle;

	private String reportType;//bar-chart, pie-chart, etc
	
	private List<Col> cols;
	
	private List<Row> rows;
	
	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the reportTitle
	 */
	public String getReportTitle() {
		return reportTitle;
	}

	/**
	 * @param reportTitle the reportTitle to set
	 */
	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	/**
	 * @param col
	 */
	public void addCol(Col col){
		if(this.cols == null){
			this.cols = new ArrayList<Col>();
		}
		this.cols.add(col);
	}
	
	public void addRow(Row row){
		if(this.rows == null){
			this.rows = new ArrayList<Row>();
		}
		this.rows.add(row);
	}
	/**
	 * @return the cols
	 */
	public List<Col> getCols() {
		return cols;
	}

	/**
	 * @param cols the cols to set
	 */
	public void setCols(List<Col> cols) {
		this.cols = cols;
	}

	/**
	 * @return the rows
	 */
	public List<Row> getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(List<Row> rows) {
		this.rows = rows;
	}
	
	
	
}
