/**
 * 
 */
package org.zcode.lexec.json;

import java.util.List;
import java.util.Map;

/**
 * @author fkhan
 *
 */
public class BpelRequest {

	private String userName;
	
	private String password;
	
	private String env;
	
	private String faultId;

	private List<BpelVariable> modifiedVariables;
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the faultId
	 */
	public String getFaultId() {
		return faultId;
	}

	/**
	 * @param faultId the faultId to set
	 */
	public void setFaultId(String faultId) {
		this.faultId = faultId;
	}

	/**
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}

	/**
	 * @param env the env to set
	 */
	public void setEnv(String env) {
		this.env = env;
	}
	

	/**
	 * @return the modifiedVariables
	 */
	public List<BpelVariable> getModifiedVariables() {
		return modifiedVariables;
	}

	/**
	 * @param modifiedVariables the modifiedVariables to set
	 */
	public void setModifiedVariables(List<BpelVariable> modifiedVariables) {
		this.modifiedVariables = modifiedVariables;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BpelRequest [userName=" + userName + ", env=" + env + ", faultId=" + faultId + "]";
	}
	
	
}
