package org.zcode.lexec.json;

/**
 * @author Faisal_Khan01
 * Oct 13, 2016
 *
 */
public class SubTask {

    private String type;

    private String taskName;

    private String description;
    
    private String inputParams;

    private String database;

    private String outParam;

    private String expression="";

    private String url;

    private String inputRequest;

    private String sqlQuery;
    
    /**
     * @return the sqlQuery
     */
    public String getSqlQuery() {
        return sqlQuery;
    }

    /**
     * @param sqlQuery the sqlQuery to set
     */
    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * @param taskName the taskName to set
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * @return the inputParams
     */
    public String getInputParams() {
        return inputParams;
    }

    /**
     * @param inputParams the inputParams to set
     */
    public void setInputParams(String inputParams) {
        this.inputParams = inputParams;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

    /**
     * @param database the database to set
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * @return the outParam
     */
    public String getOutParam() {
        return outParam;
    }

    /**
     * @param outParam the outParam to set
     */
    public void setOutParam(String outParam) {
        this.outParam = outParam;
    }

    /**
     * @return the expression
     */
    public String getExpression() {
        return expression;
    }

    /**
     * @param expression the expression to set
     */
    public void setExpression(String expression) {
        this.expression = expression;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the inputRequest
     */
    public String getInputRequest() {
        return inputRequest;
    }

    /**
     * @param inputRequest the inputRequest to set
     */
    public void setInputRequest(String inputRequest) {
        this.inputRequest = inputRequest;
    }

    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "SubTask [type=" + type + ", taskName=" + taskName
		+ ", description=" + description + ", inputParams="
		+ inputParams + ", database=" + database + ", outParam="
		+ outParam + ", expression=" + expression + ", url=" + url
		+ ", inputRequest=" + inputRequest + ", sqlQuery=" + sqlQuery
		+ "]";
    }

    
}
