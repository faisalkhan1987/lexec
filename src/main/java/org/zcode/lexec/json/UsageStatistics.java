/**
 * 
 */
package org.zcode.lexec.json;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fkhan
 *
 */
public class UsageStatistics {

	private Map<String, Integer> workaroundStatistics;
	
	private Map<String, Integer> userStatistics;

	/**
	 * @return the workaroundStatistics
	 */
	public Map<String, Integer> getWorkaroundStatistics() {
		if(workaroundStatistics == null){
			workaroundStatistics = new HashMap<>();
		}
		return workaroundStatistics;
	}

	/**
	 * @param workaroundStatistics the workaroundStatistics to set
	 */
	public void setWorkaroundStatistics(Map<String, Integer> workaroundStatistics) {
		this.workaroundStatistics = workaroundStatistics;
	}

	/**
	 * @return the userStatistics
	 */
	public Map<String, Integer> getUserStatistics() {
		if(userStatistics == null){
			userStatistics = new HashMap<>();
		}
		return userStatistics;
	}

	/**
	 * @param userStatistics the userStatistics to set
	 */
	public void setUserStatistics(Map<String, Integer> userStatistics) {
		this.userStatistics = userStatistics;
	}
	
	
}
