/**
 * 
 */
package org.zcode.lexec.json;

/**
 * @author fkhan
 *
 */
public class C {
	private String v;
	private String f;
	/**
	 * @return the v
	 */
	public String getV() {
		return v;
	}
	/**
	 * @param v the v to set
	 */
	public void setV(String v) {
		this.v = v;
	}
	/**
	 * @return the f
	 */
	public String getF() {
		return f;
	}
	/**
	 * @param f the f to set
	 */
	public void setF(String f) {
		this.f = f;
	}
	
	
}