package org.zcode.lexec.json;

import org.bson.types.ObjectId;

/**
 * @author Faisal_Khan01
 * Oct 13, 2016
 *
 */
public class ExecResponse extends AsyncResponse{

	private String threadId;
	
    private String status;
    
    private String errorMessage;

    public ExecResponse(){
    	super();
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

	/**
	 * @return the threadId
	 */
	public String getThreadId() {
		return threadId;
	}

	/**
	 * @param threadId the threadId to set
	 */
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	
    
    
}
