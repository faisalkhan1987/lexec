/**
 * 
 */
package org.zcode.lexec.json;

import java.util.Map;

import org.bson.types.ObjectId;

/**
 * @author fkhan
 *
 */
public class BpelResponse extends AsyncResponse{
	
	private String faultTrace;
	
//	private String faultSummary;
	
	private String auditTrail;
	
	private Map<String, String> bpelVariables = null;
	
	private String response;

	private String errorMessage;
	
	private String threadId;
	
	public BpelResponse(){
		super();
	}
	
	/**
	 * @return the faultTrace
	 */
	public String getFaultTrace() {
		return faultTrace;
	}

	/**
	 * @param faultTrace the faultTrace to set
	 */
	public void setFaultTrace(String faultTrace) {
		this.faultTrace = faultTrace;
	}

	/**
	 * @return the auditTrail
	 */
	public String getAuditTrail() {
		return auditTrail;
	}

	/**
	 * @param auditTrail the auditTrail to set
	 */
	public void setAuditTrail(String auditTrail) {
		this.auditTrail = auditTrail;
	}

	/**
	 * @return the bpelVariables
	 */
	public Map<String, String> getBpelVariables() {
		return bpelVariables;
	}

	/**
	 * @param bpelVariables the bpelVariables to set
	 */
	public void setBpelVariables(Map<String, String> bpelVariables) {
		this.bpelVariables = bpelVariables;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the threadId
	 */
	public String getThreadId() {
		return threadId;
	}

	/**
	 * @param threadId the threadId to set
	 */
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

//	/**
//	 * @return the faultSummary
//	 */
//	public String getFaultSummary() {
//		return faultSummary;
//	}
//
//	/**
//	 * @param faultSummary the faultSummary to set
//	 */
//	public void setFaultSummary(String faultSummary) {
//		this.faultSummary = faultSummary;
//	}
	
	

}
