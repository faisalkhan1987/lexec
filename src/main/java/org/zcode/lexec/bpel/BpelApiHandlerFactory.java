/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

import java.util.List;

/**
 *
 * @author fkhan
 */
public class BpelApiHandlerFactory {
 
    public static <E> AbstractBpelApiHandler getBPELApiHandler(String providerURL, String userName, String password, E e) throws Exception{
        
        if(e instanceof String){
            String text = (String)e;
            if(text == null || text.length() <= 0){
                throw new Exception("Text cannot be null or empty");
            }
            if(text.startsWith("wls")){
                return new BPELFaultHandler(providerURL, userName, password);
            }else{
                return new BPELInstanceHandler(providerURL, userName, password);
            }
        }else if (e instanceof List){
            return new BPELBulkFaultHandler(providerURL, userName, password);
        }else {
            throw new Exception("No Handler found for the given input data"+e);
        }
    }
}
