/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import oracle.soa.management.facade.CompositeInstance;
import oracle.soa.management.facade.Fault;
import oracle.soa.management.facade.FaultRecoveryActionTypeConstants;
import oracle.soa.management.util.CompositeInstanceFilter;
import oracle.soa.management.util.FaultFilter;

/**
 *
 * This handler works based on BPEL instance ID.
 *
 * @author fkhan
 */
public class BPELInstanceHandler extends AbstractBpelApiHandler<String> {
	
	static final Logger logger = Logger.getLogger(BPELInstanceHandler.class);
	
    public BPELInstanceHandler(String providerURL, String userName, String password) {
        super(providerURL, userName, password);
    }

    @Override
    public void resubmitBPEL(String text) throws Exception {
    	try{
    		
            init();
            String[] instanceIDs = text.split(",");
            for (String instanceID : instanceIDs) {
            	logger.debug("Retrieving ECID for instance : " + instanceID);
                List<String> ecIDs = getECIDs(instanceID);
                if(!ecIDs.isEmpty()){
                    
                    for (String ecID : ecIDs) {
                        if (ecID != null && ecID.length() > 0) {
                            List<oracle.soa.management.facade.Fault> bpelFaults = null;
                            FaultFilter filterFaults = new FaultFilter();
                            filterFaults.setECID(ecID);

                            bpelFaults = bpel.getFaults(filterFaults);

                            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                            	logger.debug("Replaying ECID : " + ecID);
                                for (Fault fault : bpelFaults) {
                                    bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_REPLAY_SCOPE, "");                                    
                                }
                            	logger.debug("Replay Completed for ECID : "+ecID);
                            } else {
                            	logger.debug("No bpel faults found with the ECID : " + ecID);
                            }
                        } else {
                        	logger.debug("No ECID found for BPEL instance or ECID is Null : " + instanceID);
                        }
                    }
                }else{
                	logger.debug("No ECID found for BPEL instance : " + instanceID);
                }
            }
    	}finally{
    		closeLocator();
    	}
    }
    
    @Override
    public void retryBPEL(String text, Map<String,String> modifiedVariables) throws Exception {
    	try{
            init();
            String[] instanceIDs = text.split(",");
            for (String instanceID : instanceIDs) {
            	logger.debug("Retrieving ECID for instance : " + instanceID);
                List<String> ecIDs = getECIDs(instanceID);
                if(!ecIDs.isEmpty()){
                    
                    for (String ecID : ecIDs) {
                        if (ecID != null && ecID.length() > 0) {
                            List<oracle.soa.management.facade.Fault> bpelFaults = null;
                            FaultFilter filterFaults = new FaultFilter();
                            filterFaults.setECID(ecID);

                            bpelFaults = bpel.getFaults(filterFaults);
                            
                            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                            	logger.debug("Replaying ECID : " + ecID);
                                for (Fault fault : bpelFaults) {
                                	if(modifiedVariables != null && !modifiedVariables.isEmpty()){
                        				for(String variable : modifiedVariables.keySet()){
                        					bpel.setVariable(fault, variable, modifiedVariables.get(variable));
                        				}
                                	}
                                    bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_RETRY, "");
                                }
                            	logger.debug("Replay Completed for ECID : "+ecID);
                            } else {
                            	logger.debug("No bpel faults found with the ECID : " + ecID);
                            }
                        } else {
                        	logger.debug("No ECID found for BPEL instance : " + instanceID);
                        }
                    }
                }else{
                	logger.debug("No ECID found for BPEL instance : " + instanceID);
                }
            }
    		
    	}finally{
    		closeLocator();
    	}
    }

    @Override
    public void continueBPEL(String text) throws Exception {
        try{
            init();
            String[] instanceIDs = text.split(",");

            List<oracle.soa.management.facade.Fault> bpelFaults = null;
            FaultFilter faultFilter = new FaultFilter();
            for (String instanceID : instanceIDs) {
                List<String> ecIDs = getECIDs(instanceID);
                if(!ecIDs.isEmpty()){
                    for (String ecID : getECIDs(instanceID)) {
                        if (ecID != null && ecID.length() > 0) {
                            faultFilter.setECID(ecID);

                            bpelFaults = bpel.getFaults(faultFilter);

                            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                                for (Fault fault : bpelFaults) {
                                    bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_CONTINUE, "");
                                }
                            	logger.debug("Continue Completed for ECID : "+ecID);
                            } else {
                            	logger.debug("No bpel faults found with ECID : " + ecID);
                            }
                        } else {
                        	logger.debug("ECID is null for instance : " + instanceID);
                        }
                    }
                }else{
                	logger.debug("No ECIDs found for instance : " + instanceID);
                }

                Thread.sleep(BPELConstants.SLEEP_TIME);
            }
        } finally {
        	closeLocator();
        }
    }

    @Override
    public void abortBPEL(String text) throws Exception {
    	logger.debug("Retrieving ECID for instance : " + text);
        try{
            init();
        	String[] instanceIDs = text.split(",");

            for (String instanceID : instanceIDs) {
                CompositeInstanceFilter compositeInstanceFilter = new CompositeInstanceFilter();
                compositeInstanceFilter.setId(instanceID.trim());
                List<CompositeInstance> listCompositeInstances = locator
                        .getCompositeInstances(compositeInstanceFilter);
                if (!listCompositeInstances.isEmpty()) {
                	logger.debug("!listCompositeInstances.isEmpty()" + instanceID);
                    CompositeInstance compositeInstance = listCompositeInstances.get(0);

                    compositeInstance.abort();
                } else {
                	logger.debug("listCompositeInstances.isEmpty" + instanceID);
                }
                Thread.sleep(BPELConstants.SLEEP_TIME);
            }
        }finally{
        	closeLocator();
        }
        
    }

    @Override
    public void recoverBPEL(String text) throws Exception {
    	logger.debug("recoverBPEL : " + text);
        try{
            init();
            String[] instanceIDs = text.split(",");

            for (String instanceID : instanceIDs) {
                
                List<String> ecIDs = getECIDs(instanceID);
                if(!ecIDs.isEmpty()){
                    for (String ecID : getECIDs(instanceID)) {
                        int count = 0;
                        while (hasInstancesToRecover(ecID, bpel) && count++ < 3) { //BPELConstants.MAX_RETRY_COUNT) {
                            bpel.recoverActivities(instancesToRecover);
                            Thread.sleep(BPELConstants.SLEEP_TIME);
                        }
                    }
                }else{
                	logger.debug("ecIDs.isEmpty() " + instanceID);
                }
                Thread.sleep(BPELConstants.SLEEP_TIME);
            }        	
        }finally{
        	closeLocator();
        }
    }

    @Override
    public Map<String, String> fetchDetails(String text) throws Exception {
    	logger.debug("Retrieving ECID for instance : " + text);
    	try{
            init();
            String[] instanceIDs = text.split(",");

            String ecID = getECID(instanceIDs[0]);
            if (ecID == null || ecID.length() <= 0) {
                throw new Exception("No ECID found for the bpel instance : " + instanceIDs[0]);
            }
            Map<String, String> variableMap = new HashMap<String, String>();
            List<oracle.soa.management.facade.Fault> bpelFaults = null;
            FaultFilter faultFilter = new FaultFilter();
            faultFilter.setECID(ecID);
            
            bpelFaults = bpel.getFaults(faultFilter);
        	logger.debug("bpelFaults : " + bpelFaults);
//            OutLogger.getInstance().logInfo(userName, "Fault Count : "+bpelFaults.size());
            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                Fault fault = bpelFaults.get(0);

                String[] variableNames = bpel.getVariableNames(fault);
                
                for (String variableName : variableNames) {
                	logger.debug("variableName : " + variableName);
//                    OutLogger.getInstance().logInfo(userName, "Variable Name : " + variableName);
                    try {
                        String variablePayload = bpel.getVariable(fault, variableName);
                        variableMap.put(variableName, variablePayload);
                    } catch (Exception ex) {
                        variableMap.put(variableName, "Exception occurred on data retrieval");
                    }
                }
                variableMap.put(BPELConstants.FAULT_TRACE, fault.getMessage().toString());
//                variableMap.put(BPELConstants.ECID, fault.getECID());
//                variableMap.put(BPELConstants.COMPOSITE_INSTANCE_ID, fault.getComponentInstanceId());
//                variableMap.put(BPELConstants.AUDIT_TRAIL, fetchAuditTrailForECID(fault.getECID()));
            } else {
            	logger.debug("Retrieving ECID for instance : " + instanceIDs[0]);
//                OutLogger.getInstance().logInfo(userName, "No bpel faults found for : " + instanceIDs[0]);
                variableMap.put(BPELConstants.AUDIT_TRAIL, fetchAuditTrailForECID(getECID(instanceIDs[0])));
            }
            return variableMap;    		
    	}finally{
    		closeLocator();
    	}
    }

	/* (non-Javadoc)
	 * @see org.zcode.lexec.bpel.AbstractBpelApiHandler#resubmitBPEL(java.lang.String, java.util.Map)
	 */
	@Override
	public void resubmitBPEL(String text, Map<String, String> modifiedVariables) throws Exception {
		try{
            init();
            String[] instanceIDs = text.split(",");
            for (String instanceID : instanceIDs) {
            	logger.debug("Retrieving ECID for instance : " + instanceID);
                List<String> ecIDs = getECIDs(instanceID);
                if(!ecIDs.isEmpty()){
                    
                    for (String ecID : ecIDs) {
                        if (ecID != null && ecID.length() > 0) {
                            List<oracle.soa.management.facade.Fault> bpelFaults = null;
                            FaultFilter filterFaults = new FaultFilter();
                            filterFaults.setECID(ecID);

                            bpelFaults = bpel.getFaults(filterFaults);
                            
                            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                            	logger.debug("Replaying ECID : " + ecID);
                                for (Fault fault : bpelFaults) {
                                	if(modifiedVariables != null && !modifiedVariables.isEmpty()){
	                    				for(String variable : modifiedVariables.keySet()){
	                    					bpel.setVariable(fault, variable, modifiedVariables.get(variable));
	                    				}
                                	}
                                    bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_REPLAY_SCOPE, "");
                                }
                            	logger.debug("Replay Completed for ECID : "+ecID);
                            } else {
                            	logger.debug("No bpel faults found with the ECID : " + ecID);
                            }
                        } else {
                        	logger.debug("No ECID found for BPEL instance : " + instanceID);
                        }
                    }
                }else{
                	logger.debug("No ECID found for BPEL instance : " + instanceID);
                }
            }
    		
    	}finally{
    		closeLocator();
    	}
		
	}

}
