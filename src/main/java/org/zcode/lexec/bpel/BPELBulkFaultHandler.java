/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

import java.util.List;
import java.util.Map;

import oracle.soa.management.facade.Fault;
import oracle.soa.management.facade.FaultRecoveryActionTypeConstants;
import oracle.soa.management.util.FaultFilter;

/**
 * BulkFault Handler that intakes a list of faultIDs and handles them in bulk
 * @author fkhan
 */
public class BPELBulkFaultHandler extends AbstractBpelApiHandler<List<String>> {

    public BPELBulkFaultHandler(String providerURL, String userName, String password) {
        super(providerURL, userName, password);
    }

    @Override
    public void resubmitBPEL(List<String> faultIDs) throws Exception {
        init();
        List<oracle.soa.management.facade.Fault> bpelFaults = null;
        FaultFilter filterFaults = new FaultFilter();
        for (String faultID : faultIDs) {
            filterFaults.setId(faultID);

            bpelFaults = bpel.getFaults(filterFaults);

            if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
                Fault fault = bpelFaults.get(0);

//                OutLogger.getInstance().logInfo(userName, "Replaying FaultID : " + faultID);
                bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_REPLAY_SCOPE, "");
//                OutLogger.getInstance().logInfo(userName, "Replay Completed for FaultID : "+faultID);
            } else {
//                OutLogger.getInstance().logError(userName,"No bpel faults found with the FaultID : " + faultID);
            }
//            Thread.sleep(BPELConstants.SLEEP_TIME);
        }
    }

    @Override
    public void continueBPEL(List<String> faultIDs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abortBPEL(List<String> faultIDs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void recoverBPEL(List<String> faultIDs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map fetchDetails(List<String> faultIDs) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	/* (non-Javadoc)
	 * @see org.zcode.lexec.selfcarev2.bpel.AbstractBpelApiHandler#retryBPEL(java.lang.String, java.util.Map)
	 */
	@Override
	public void retryBPEL(String text, Map<String, String> modifiedVariables) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/* (non-Javadoc)
	 * @see org.zcode.lexec.bpel.AbstractBpelApiHandler#resubmitBPEL(java.lang.String, java.util.Map)
	 */
	@Override
	public void resubmitBPEL(String text, Map<String, String> modifiedVariables) throws Exception {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		
	}

}
