/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.Context;

import org.apache.log4j.Logger;

import oracle.soa.management.facade.ActivityInstance;
import oracle.soa.management.facade.ComponentInstance;
import oracle.soa.management.facade.CompositeInstance;
import oracle.soa.management.facade.Locator;
import oracle.soa.management.facade.LocatorFactory;
import oracle.soa.management.internal.facade.bpel.BPELServiceEngineImpl;
import oracle.soa.management.util.ActivityInstanceFilter;
import oracle.soa.management.util.ComponentInstanceFilter;
import oracle.soa.management.util.CompositeInstanceFilter;

/**
 *
 * @author fkhan
 */
public abstract class AbstractBpelApiHandler<E> {
	
	static final Logger logger = Logger.getLogger(AbstractBpelApiHandler.class);
	
    protected Locator locator = null;

    protected String providerURL;
    protected String userName;
    protected String password;
    protected BPELServiceEngineImpl bpel;
    protected List<ActivityInstance> instancesToRecover = null;
    
    public AbstractBpelApiHandler(String providerURL, String userName, String password){
        this.providerURL = providerURL;
        this.userName = userName;
        this.password = password;
    }

    public abstract void resubmitBPEL(E e) throws Exception;
    
    public abstract void resubmitBPEL(String text, Map<String, String> modifiedVariables) throws Exception;

    public abstract void continueBPEL(E e) throws Exception;

    public abstract void abortBPEL(E e) throws Exception;

    public abstract void recoverBPEL(E e) throws Exception;

    public abstract void retryBPEL(String text, Map<String, String> modifiedVariables) throws Exception;
    
    public abstract Map<String, String> fetchDetails(E e) throws Exception;

    
    public void init() throws Exception {
        Hashtable jndiProps = new Hashtable();
        jndiProps
                .put(Context.PROVIDER_URL, providerURL);
        jndiProps.put(Context.INITIAL_CONTEXT_FACTORY,
                "weblogic.jndi.WLInitialContextFactory");
        jndiProps.put(Context.SECURITY_PRINCIPAL, userName);
        jndiProps.put(Context.SECURITY_CREDENTIALS, password);
        jndiProps.put("dedicated.connection", "false");
        locator = LocatorFactory.createLocator(jndiProps);
        bpel = (BPELServiceEngineImpl) locator.getServiceEngine("bpel");
    }

    protected void closeLocator(){
    	if(locator != null){
    		locator.close();
    	}
    }
    protected String fetchAuditTrailForECID(String ecID) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        CompositeInstanceFilter filter = new CompositeInstanceFilter();
        filter.setECID(ecID);
        List<CompositeInstance> compositeInstances = locator.getCompositeInstances(filter);

        if (compositeInstances != null && !compositeInstances.isEmpty()) {
            CompositeInstance compositeInstance = compositeInstances.get(0);
            logger.debug("ECID of Composite Instance : " + compositeInstance.getECID());

            ComponentInstanceFilter componentInstanceFilter = new ComponentInstanceFilter();
            componentInstanceFilter.setECID(compositeInstance.getECID());
            List<ComponentInstance> componentInstanceList = compositeInstance.getChildComponentInstances(componentInstanceFilter);

            logger.debug("Component Instance List Size : " + componentInstanceList.size());
            for (ComponentInstance componentInstance : componentInstanceList) {
                stringBuilder.append(componentInstance.getAuditTrail().toString());
            }
        }
        locator.close();
        return stringBuilder.toString();
    }

    protected List<String> getECIDs(String instanceID) throws Exception {
        logger.debug("Retrieving ECID for instance : " + instanceID);
        String ecID = null;
        CompositeInstanceFilter filter = new CompositeInstanceFilter();
        filter.setId(instanceID.trim());
        List<CompositeInstance> compositeInstances = locator.getCompositeInstances(filter);
        List<String> ecIDs = new ArrayList<String>();
        for (CompositeInstance compositeInstance : compositeInstances) {
            ecID = compositeInstance.getECID();
            logger.debug("ECID : " + ecID);
            ecIDs.add(ecID);
        }        
        return ecIDs;
    }

    protected String getECID(String instanceID) throws Exception {
        logger.debug("Retrieving ECID for instance : " + instanceID);
        String ecID = null;
        CompositeInstanceFilter filter = new CompositeInstanceFilter();
        filter.setId(instanceID.trim());
        List<CompositeInstance> compositeInstances = locator.getCompositeInstances(filter);
        for (CompositeInstance compositeInstance : compositeInstances) {
            ecID = compositeInstance.getECID();
            logger.debug("ECID : " + ecID);
        }        
        return ecID;
    }
    
    protected boolean hasInstancesToRecover(String ecID, BPELServiceEngineImpl bpel) throws Exception {
        if (ecID == null || ecID.length() <= 0) {
            return false;
        }
        instancesToRecover = new ArrayList<ActivityInstance>();
        ActivityInstanceFilter filter = new ActivityInstanceFilter();
        filter.setECID(ecID);
        List<ActivityInstance> activityInstanceList = bpel.getActivities(filter);
        if (activityInstanceList == null || activityInstanceList.size() <= 0) {
        	logger.debug("No recoverable activities found for ECID : " + ecID);
            return false;
        }
        for (ActivityInstance instance : activityInstanceList) {
            if (instance.getState() == ActivityInstance.STATE_OPEN_ACTIVE  
                    || instance.getState() == ActivityInstance.STATE_OPEN_PENDING_COMPLETE  
                    || instance.getState() == ActivityInstance.STATE_OPEN_PENDING_RECOVERY) {
                instancesToRecover.add(instance);
            }
        }
        if (instancesToRecover.size() > 0) {
            return true;
        } else {
        	logger.debug("No instances to recover");
        }
        return false;
    }
}
