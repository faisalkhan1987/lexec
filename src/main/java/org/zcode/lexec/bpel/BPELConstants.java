/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

/**
 *
 * @author fkhan
 */
public class BPELConstants {

    public static long SLEEP_TIME = 1000;

    public static final String MESSAGE = "0_MESSAGE";
    public static final String AUDIT_TRAIL = "1_AUDIT__TRAIL";
    public static final String FAULT_TRACE = "2_FAULT___TRACE";
    public static final String COMPOSITE_INSTANCE_ID = "3_COMPOSITE__INSTANCE__ID";
    public static final String ECID = "4_ECID";
    
    public static final int MAX_RETRY_COUNT = 5;

    public static final String PROVIDER_URL = "t3://bplmngdcluster.env.be:8701/soa-infra";
    
    public static final String BPEL_USERNAME = "#BPEL_USERNAME";
    public static final String BPEL_PASSWORD = "#BPEL_PASSWORD";
}
