/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zcode.lexec.bpel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.soa.management.facade.Fault;
import oracle.soa.management.facade.FaultRecoveryActionTypeConstants;
import oracle.soa.management.util.FaultFilter;

/**
 *
 * @author fkhan
 */
public class BPELFaultHandler extends AbstractBpelApiHandler<String> {

	List<oracle.soa.management.facade.Fault> bpelFaults = null;
	FaultFilter faultFilter = null;

	public BPELFaultHandler(String providerURL, String userName, String password) {
		super(providerURL, userName, password);
		faultFilter = new FaultFilter();
	}

	@Override
	public void resubmitBPEL(String faultID) throws Exception {
		init();
		// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : " +
		// faultID);
		faultFilter.setId(faultID);

		bpelFaults = bpel.getFaults(faultFilter);

		if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
			Fault fault = bpelFaults.get(0);

			// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : "
			// + faultID);
			bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_REPLAY_SCOPE, "");
			// OutLogger.getInstance().logInfo(userName, "Replay Completed for
			// FaultID : "+faultID);
		} else {
			// OutLogger.getInstance().logError(userName, "No bpel faults found
			// with the FaultID : " + faultID);
		}
	}

	@Override
	public void continueBPEL(String faultID) throws Exception {
		init();
		// OutLogger.getInstance().logInfo(userName,"Continuing FaultID : " +
		// faultID);

		faultFilter.setId(faultID);
		bpelFaults = bpel.getFaults(faultFilter);

		if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
			Fault fault = bpelFaults.get(0);

			// OutLogger.getInstance().logInfo(userName,"Continuing FaultID : "
			// + faultID);
			bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_CONTINUE, "");
			// OutLogger.getInstance().logInfo(userName, "Continue Completed for
			// FaultID : "+faultID);
		} else {
			// OutLogger.getInstance().logError(userName, "No bpel faults found
			// with FaultID : " + faultID);
		}
	}

	@Override
	public void abortBPEL(String faultID) {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}

	@Override
	public void recoverBPEL(String faultID) {
		throw new UnsupportedOperationException("Not supported yet."); // To
																		// change
																		// body
																		// of
																		// generated
																		// methods,
																		// choose
																		// Tools
																		// |
																		// Templates.
	}

	@Override
	public Map<String, String> fetchDetails(String faultID) throws Exception {
		init();
		// OutLogger.getInstance().logInfo(userName,"Fetch Fault Details for : "
		// + faultID);

		Map<String, String> variableMap = new HashMap<String, String>();

		faultFilter.setId(faultID);
		bpelFaults = bpel.getFaults(faultFilter);

		if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
			Fault fault = bpelFaults.get(0);

			String[] variableNames = bpel.getVariableNames(fault);
			for (String variableName : variableNames) {
				// OutLogger.getInstance().logInfo(userName, "Fetching payload
				// for Variable : " + variableName);
				try {
					String variablePayload = bpel.getVariable(fault, variableName);
					variableMap.put(variableName, variablePayload);
				} catch (Exception ex) {
					variableMap.put(variableName, "Exception occurred on data retrieval");
				}
			}
			variableMap.put(BPELConstants.FAULT_TRACE, fault.getMessage().toString());
//			variableMap.put(BPELConstants.ECID, fault.getECID());
//			variableMap.put(BPELConstants.COMPOSITE_INSTANCE_ID, fault.getComponentInstanceId());
//			variableMap.put(BPELConstants.AUDIT_TRAIL, fetchAuditTrailForECID(fault.getECID()));
		} else {
			// OutLogger.getInstance().logError(userName, "No bpel faults found
			// for : " + faultID);
			variableMap.put(BPELConstants.MESSAGE, "No bpel faults");
		}
		return variableMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.zcode.lexec.selfcarev2.bpel.AbstractBpelApiHandler#retryBPEL(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void retryBPEL(String text, Map<String, String> modifiedVariables) throws Exception {
		init();
		// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : " +
		// faultID);
		faultFilter.setId(text);

		bpelFaults = bpel.getFaults(faultFilter);

		if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
			for (Fault fault : bpelFaults) {
            	if(modifiedVariables != null && !modifiedVariables.isEmpty()){
					for(String variable : modifiedVariables.keySet()){
						bpel.setVariable(fault, variable, modifiedVariables.get(variable));
					}
            	}
			// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : "
					
			// + faultID);
				bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_RETRY, "");
			}
			// OutLogger.getInstance().logInfo(userName, "Replay Completed for
			// FaultID : "+faultID);
		} else {
			// OutLogger.getInstance().logError(userName, "No bpel faults found
			// with the FaultID : " + faultID);
		}
	}

	/* (non-Javadoc)
	 * @see org.zcode.lexec.bpel.AbstractBpelApiHandler#resubmitBPEL(java.lang.String, java.util.Map)
	 */
	@Override
	public void resubmitBPEL(String text, Map<String, String> modifiedVariables) throws Exception {
		init();
		// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : " +
		// faultID);
		faultFilter.setId(text);

		bpelFaults = bpel.getFaults(faultFilter);

		if ((bpelFaults != null) && !bpelFaults.isEmpty()) {
			for (Fault fault : bpelFaults) {
            	if(modifiedVariables != null && !modifiedVariables.isEmpty()){
				
					for(String variable : modifiedVariables.keySet()){
						bpel.setVariable(fault, variable, modifiedVariables.get(variable));
					}
            	}
			// OutLogger.getInstance().logInfo(userName, "Replaying FaultID : "
			// + faultID);
				bpel.recoverFault(fault, FaultRecoveryActionTypeConstants.ACTION_REPLAY_SCOPE, "");
			}
			// OutLogger.getInstance().logInfo(userName, "Replay Completed for
			// FaultID : "+faultID);
		} else {
			// OutLogger.getInstance().logError(userName, "No bpel faults found
			// with the FaultID : " + faultID);
		}
		
	}

}
