package org.zcode.lexec.builder;

import org.zcode.lexec.domain.Task;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;

/**
 * @author Faisal_Khan01
 * Oct 13, 2016
 *
 */
public interface ITaskBuilder {

    Task buildTask(Strategy strategy);
    
    Task buildTask(SubTask subTask);
}
