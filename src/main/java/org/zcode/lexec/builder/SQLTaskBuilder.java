package org.zcode.lexec.builder;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;
import org.zcode.lexec.sqlx.dao.DSConfigDao;

/**
 * @author Faisal_Khan01 Oct 13, 2016
 *
 */
@Component
public class SQLTaskBuilder implements ITaskBuilder {

	@Autowired
	DSConfigDao dsConfigDao;

	@Override
	public Task buildTask(Strategy strategy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task buildTask(SubTask subTask) {
		SQLTask task = new SQLTask();
		task.setName(subTask.getTaskName());
		task.setDescription(subTask.getDescription());
		if (subTask.getInputParams() != null) {
			task.setInputParams(Arrays.asList(subTask.getInputParams().split(",")));
		}
		task.setOutParam(subTask.getOutParam());
		task.setQueryText(subTask.getSqlQuery());
		// System.out.println("***********" + dsConfigDao);
		task.setDbName(subTask.getDatabase());
		// task.setDsConfig(dsConfigDao.findByDsName(subTask.getDatabase()));
		return task;
	}

}
