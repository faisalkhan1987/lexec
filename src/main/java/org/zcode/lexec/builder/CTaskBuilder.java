package org.zcode.lexec.builder; 

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.AlertTask;
import org.zcode.lexec.domain.BpelTask;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.domain.BpelTask.BpelAction;
import org.zcode.lexec.factory.TaskBuilderFactory;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;

/**
 * @author Faisal_Khan01 Oct 13, 2016
 *
 */
@Component
public class CTaskBuilder implements ITaskBuilder {

	@Resource
	TaskBuilderFactory taskBuilderFactory;

	@Override
	public Task buildTask(Strategy strategy) {
		CTask cTask = new CTask();
		cTask.setName(strategy.getName());
		cTask.setDescription(strategy.getDescription());
		cTask.setInputParams(strategy.getInputParams());
		cTask.setPasswordParams(strategy.getPasswordParams());
		cTask.setGroup(strategy.getGroup());
		cTask.setApproved(false);
		cTask.setApprovedForBulkExec(false);
		List<SubTask> subTasks = strategy.getTasks();
		cTask.setChildTasks(buildChildTasks(subTasks));
		return cTask;
	}

	private List<Task> buildChildTasks(List<SubTask> subTaskList){
		List<Task> taskList = new ArrayList<>();
		Task task = null;
		for(int i = 0;i < subTaskList.size(); i++){
			SubTask subTask = subTaskList.get(i);
			
			if(subTask.getExpression().startsWith("if")){
				CTask cTask = new CTask();
				cTask.setExpression(subTask.getExpression());
				cTask.setChildTasks(buildChildTasks(subTaskList.subList(i+1, subTaskList.size())));
				
				i = i + cTask.getChildTasks().size();
				taskList.add(cTask);
				
			} else if(subTask.getExpression().startsWith("endif")){
				return taskList;
			} else if(subTask.getExpression().startsWith("alert")){
				task = new AlertTask();
				((AlertTask)task).setMessage(subTask.getExpression());
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelresubmit")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.RESUBMIT);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelcontinue")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.CONTINUE);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelrecover")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.RECOVER);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelabort")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.ABORT);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelretry")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.RETRY);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelfetchfault")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.FETCH_FAULT_DETAILS);
				String[] params = subTask.getExpression().trim().split(" ");
				((BpelTask)task).setInstanceVariable(params[1]);
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelreplacevariable")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.REPLACE_VARIABLE);
				String expression = subTask.getExpression().trim();
				String[] params = expression.split(" ");
				((BpelTask)task).setBpelVariableName(params[1]);
				((BpelTask)task).setSourceString(params[2]);
				((BpelTask)task).setTargetString(params[3]);
				if(subTask.getInputParams() != null && !subTask.getInputParams().isEmpty()){
					task.setInputParams(Arrays.asList(subTask.getInputParams().split(",")));
				}
				taskList.add(task);
			} else if(subTask.getExpression().startsWith("bpelinsertvariable")){
				task = new BpelTask();
				((BpelTask)task).setAction(BpelAction.REPLACE_VARIABLE_INSERT);
				String expression = subTask.getExpression().trim();
				String[] params = expression.split(" ");
				((BpelTask)task).setBpelVariableName(params[1]);
				((BpelTask)task).setInsertAfterNode(params[2]);
				((BpelTask)task).setNewNode(params[3]);
				if(subTask.getInputParams() != null && !subTask.getInputParams().isEmpty()){
					task.setInputParams(Arrays.asList(subTask.getInputParams().split(",")));
				}
				taskList.add(task);
			} else{
				task = taskBuilderFactory.buildTask(subTask);
				taskList.add(task);
			}
		}
		return taskList;
	}
	
	@Override
	public Task buildTask(SubTask subTask) {
		// TODO Auto-generated method stub
		return null;
	}

}
