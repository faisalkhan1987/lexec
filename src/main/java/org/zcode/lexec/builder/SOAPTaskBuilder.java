package org.zcode.lexec.builder;

import java.util.Arrays;

import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;

/**
 * @author Faisal_Khan01
 * Oct 13, 2016
 *
 */
@Component
public class SOAPTaskBuilder implements ITaskBuilder{

    @Override
    public Task buildTask(Strategy strategy) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public Task buildTask(SubTask subTask) {
	SOAPTask task = new SOAPTask();
	task.setName(subTask.getTaskName());
	task.setDescription(subTask.getDescription());
	if(subTask.getInputParams() != null){
	    task.setInputParams(Arrays.asList(subTask.getInputParams().split(",")));
	}
	task.setOutParam(subTask.getOutParam());
	task.setWsRequest(subTask.getInputRequest());
	task.setWsEndpoint(subTask.getUrl());
	return task;
    }

}
