package org.zcode.lexec.factory;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zcode.lexec.builder.CTaskBuilder;
import org.zcode.lexec.builder.ITaskBuilder;
import org.zcode.lexec.builder.SOAPTaskBuilder;
import org.zcode.lexec.builder.SQLTaskBuilder;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;

/**
 * @author Faisal_Khan01 Oct 13, 2016
 *
 */
@Component
public class TaskBuilderFactory {

	@Resource
	ITaskBuilder CTaskBuilder;

	@Resource
	ITaskBuilder SQLTaskBuilder;

	@Resource
	ITaskBuilder SOAPTaskBuilder;
	
	public Task buildTask(Strategy strategy) {
		return CTaskBuilder.buildTask(strategy);
	}

	public Task buildTask(SubTask subTask) {
		if ("SQL".equals(subTask.getType())) {
			return SQLTaskBuilder.buildTask(subTask);
		} else if ("SOAP".equals(subTask.getType())) {
			return SOAPTaskBuilder.buildTask(subTask);
		} else if("CONTROL".equals(subTask.getType())){
			return CTaskBuilder.buildTask(subTask);
		}
		return null;
	}
}
