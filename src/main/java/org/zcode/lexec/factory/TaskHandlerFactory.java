/**
 * 
 */
package org.zcode.lexec.factory;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.zcode.lexec.domain.AlertTask;
import org.zcode.lexec.domain.BpelTask;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.engine.CustomTaskActionOrchestrator;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.handler.ITaskActionHandler;

/**
 * @author faisal_khan01 Oct 5, 2016
 *
 */
@Component
public class TaskHandlerFactory {

	static final Logger logger = Logger.getLogger(TaskHandlerFactory.class);
	
	@Resource
	ITaskActionHandler SQLTaskActionHandler;

	@Resource
	ITaskActionHandler SOAPTaskActionHandler;

	@Resource
	ITaskActionHandler CTaskActionHandler;

	@Resource
	ITaskActionHandler alertTaskActionHandler;

	@Resource
	ITaskActionHandler bpelTaskActionHandler;
	/**
	 * @param task
	 * @param symbolTable
	 * @return
	 * @throws ExecutionException
	 */
	public void processTask(Task task, String teleEnv) throws PreProcessingException, ExecutionException {
		logger.debug("processTask : " + task);
		if (task instanceof SQLTask) {
			SQLTaskActionHandler.handleTask(task, teleEnv);
		} else if (task instanceof SOAPTask) {
			SOAPTaskActionHandler.handleTask(task, teleEnv);
		} else if (task instanceof CTask) {
			CTaskActionHandler.handleTask(task, teleEnv);
		} else if (task instanceof AlertTask){
			alertTaskActionHandler.handleTask(task, teleEnv);
		} else if (task instanceof BpelTask){
			bpelTaskActionHandler.handleTask(task, teleEnv);
		}

	}
	
}
