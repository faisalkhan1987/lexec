/**
 * 
 */
package org.zcode.lexec.constants;

/**
 * @author fkhan
 *
 */
public interface CommonConstants {

	public static final String TELE_ENV = "${teleEnv}";
	
	public static final String ROLE_ADMIN = "ADMIN";
	
	public static final String ROLE_SUPER_ADMIN = "SUPER_ADMIN";
	
	public static final String ROLE_USER = "USER";
}
