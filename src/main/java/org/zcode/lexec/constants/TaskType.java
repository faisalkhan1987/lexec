package org.zcode.lexec.constants;

/**
 * @author Faisal_Khan01
 * Sep 23, 2016
 *
 */
public enum TaskType {
    
    _TYPE_SQL, _TYPE_WS, _TYPE_COMPOSITE
    
}
