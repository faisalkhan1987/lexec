package org.zcode.lexec.constants;


/**
 * @author faisal_khan01
 *
 */
public interface SQLConstants {

	public static enum TransactionMode {
		NO_TRANSACTION, TRANSACTION_REQUIRED
	}
}
