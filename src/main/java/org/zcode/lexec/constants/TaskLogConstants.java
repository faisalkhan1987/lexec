/**
 * 
 */
package org.zcode.lexec.constants;

/**
 * @author fkhan
 *
 */
public interface TaskLogConstants {

	public static final String ALERT = "alert";
	
	public static final String SQL_INPUT = "sqlinput";
	
	public static final String SQL_OUTPUT = "sqloutput";
	
	public static final String WS_INPUT = "wsinput";
	
	public static final String BPEL_INPUT = "bpelinput";
	
	public static final String WS_OUTPUT = "wsoutput";
	
	public static final String EXCEPTION = "exception";
	
	public static final String INFO = "info";
	
	public static final String OUTPUT = "output";
	
	public static final String ERROR = "error";
}
