/**
 * 
 */
package org.zcode.lexec.antlr4;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.zcode.lexec.antlr4.CustomListener;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarLexer;
import org.zcode.lexec.antlr4.generated.RuleSetGrammarParser;

/**
 * @author fkhan
 *
 */
public class RuleSetTest {

	public static void main(String[] args){
		CharStream in = new ANTLRInputStream("if 1=1 and (c=c and d=d)");		
		RuleSetGrammarLexer lexer = new RuleSetGrammarLexer(in);
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);	
		RuleSetGrammarParser parser = new RuleSetGrammarParser(tokenStream);
		CustomListener listener = new CustomListener();
		new ParseTreeWalker().walk(listener, parser.rule_set());
		System.out.println("Result : " +listener.getResult());
	}
}
