package org.zcode.lexec.wsx.handler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.handler.ITaskActionHandler;
import org.zcode.lexec.handler.SOAPTaskActionHandler;
import org.zcode.lexec.wsx.dao.SOAPTaskDao;
import org.zcode.lexec.wsx.vo.SOAPRequest;

/**
 * @author Faisal_Khan01
 * Sep 26, 2016
 *
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//Annotation to rollbak after save
//@Transactional
public class SOAPTaskActionHandlerTest {

//    @Autowired
//    private SOAPTaskDao wsTaskDao;
//    
//    @Test
//    public void testSoapExecFromDB() throws ExecutionException, PreProcessingException{
//	SOAPTask wsTask = wsTaskDao.findOne("57f497bbab08df07e0455593");
//	SOAPRequest soapRequest = new SOAPRequest();
//	soapRequest.setXmlRequest(wsTask.getWsRequest());
//	soapRequest.setEndPointUri(wsTask.getWsEndpoint());
//	ITaskActionHandler handler = new SOAPTaskActionHandler();
//	handler.handleTask(wsTask);
//    }
//    @Test
//    public void testSoapExecution() throws ExecutionException{
//	ITaskActionHandler<SOAPRequest, SOAPResponse> soapTaskActionHandler = new SOAPTaskActionHandler();
//	SOAPRequest request = new SOAPRequest();
//	request.setXmlRequest("<a>112312</a>");
//	request.setEndPointUri("http://xyz.api");
//	
//	SOAPResponse response = soapTaskActionHandler.handleRequest(request);
//    }
}
