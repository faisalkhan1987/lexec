/**
 * 
 */
package org.zcode.lexec.js;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.eclipsesource.v8.JavaCallback;
import com.eclipsesource.v8.NodeJS;
import com.eclipsesource.v8.V8Array;
import com.eclipsesource.v8.V8Object;

/**
 * @author fkhan
 *
 */
public class JSTest {

	public static void main(String[] args) throws FileNotFoundException, ScriptException{
		final NodeJS nodeJS = NodeJS.createNodeJS();
		JavaCallback callback = new JavaCallback() {
			
		    public Object invoke(V8Object receiver, V8Array parameters) {
		      return "Hello, JavaWorld!";
		    }
		  };
				
		  nodeJS.getRuntime().registerJavaMethod(callback, "someJavaMethod");
		nodeJS.exec(new File("C:\\Users\\fkhan\\git\\products\\selfcare\\src\\main\\js\\repgen.js"));
		while(nodeJS.isRunning()) {
		    nodeJS.handleMessage();
		  }
		  nodeJS.release();
//		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
//		engine.put("console", System.out);
//		engine.eval(new FileReader("C:\\Users\\fkhan\\git\\products\\selfcare\\src\\main\\js\\repgen.js"));
	}
}
