package org.zcode.lexec.sqlx.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.zcode.lexec.domain.DSConfig;
import org.zcode.lexec.sqlx.dao.DSConfigDao;

/**
 * @author Faisal_Khan01
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
// Annotation to rollbak after save
// @Transactional
public class DSConfigDaoTest {

    @Autowired
    private DSConfigDao dsConfigDao;

    @Test
    public void testSave() {

	DSConfig dsConfig = new DSConfig();
	dsConfig.setDsName("CDM");
	dsConfig.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	dsConfig.setUserName("SYSTEM");
	dsConfig.setEncryptedPassword("abcABC123!@#");
	dsConfig.setJdbcConnectionString("jdbc:oracle:thin:@127.0.0.1:1521:xe");

	dsConfigDao.save(dsConfig);
    }

    @Test
    public void testRetrieveByName(){
	DSConfig dsConfig = dsConfigDao.findByDsName("CDM");
	System.out.println(dsConfig);
    }
    @Test
    public void testRetrieval() {
	DSConfig dsConfig = dsConfigDao.findOne("57f497bbab08df07e0455593");
	System.out.println(dsConfig);
    }

    @Test
    public void testRetrieveAll() {
	Iterable<DSConfig> dsConfigList = dsConfigDao.findAll();
	for (DSConfig dsConfig : dsConfigList) {
	    System.out.println(dsConfig.getContextPath());
	}
    }
}
