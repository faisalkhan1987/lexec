package org.zcode.lexec.sqlx.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.zcode.lexec.domain.DSConfig;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.sqlx.dao.DSConfigDao;
import org.zcode.lexec.sqlx.dao.SQLTaskDao;

/**
 * @author Faisal_Khan01
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
// Annotation to rollbak after save
// @Transactional
public class SQLTaskDaoTest {

    @Autowired
    private SQLTaskDao sqlConfigDao;

    @Autowired
    private DSConfigDao dsConfigDao;

    @Test
    public void testSave() {

	SQLTask sqlConfig = new SQLTask();
//	DSConfig dsConfig = dsConfigDao.findOne("57f50a09ab08df2910ab3c6a");
//	sqlConfig.setDsConfig(dsConfig);
	sqlConfig.setName("Test");
	sqlConfig.setQueryText("select ${STAR} from LEXEC_SQL_TASK");
	List<String> inputParams = new ArrayList<String>();
	inputParams.add("${STAR}");
	sqlConfig.setInputParams(inputParams);
	sqlConfigDao.save(sqlConfig);
    }
}
