package org.zcode.lexec.sqlx.handler;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.engine.SymbolTable;
import org.zcode.lexec.exception.ExecutionException;
import org.zcode.lexec.exception.PreProcessingException;
import org.zcode.lexec.handler.ITaskActionHandler;
import org.zcode.lexec.handler.SQLTaskActionHandler;
import org.zcode.lexec.sqlx.dao.SQLTaskDao;
import org.zcode.lexec.sqlx.vo.SQLConnectionParams;
import org.zcode.lexec.sqlx.vo.SQLExecRequest;
import org.zcode.lexec.sqlx.vo.SQLExecResponse;
import org.zcode.lexec.sqlx.vo.SQLRow;

/**
 * @author faisal_khan01
 * 
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//Annotation to rollbak after save
//@Transactional
public class SQLTaskActionHandlerTest {//extends TestCase {

//    @Autowired
//    private SQLTaskDao sqlTaskDao;
//    
//    @Test
//    public void testExecuteFromDB() throws PreProcessingException {
//	SQLTask sqlTask = sqlTaskDao.findOne("57f50b68ab08df2a9cd22906");
//	SymbolTable.getInstance().init();
//	SymbolTable.getInstance().put("${STAR}", "*");
//	ITaskActionHandler handler = new SQLTaskActionHandler();
//
//	try {
//	    handler.handleTask(sqlTask);
//	} catch (ExecutionException e) {
//	    // TODO Auto-generated catch block
//	    e.printStackTrace();
//	}
//    }

//    @Test
//    public void testSelectQueryExecute() {
//	ITaskActionHandler handler = new SQLTaskActionHandler();
//
//	SQLExecRequest request = new SQLExecRequest();
//	SQLConnectionParams connectionParams = new SQLConnectionParams();
//	connectionParams.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//	connectionParams.setUserName("selfcare");
//	connectionParams.setPassword("selfcare123");
//	connectionParams
//		.setConnectionString("jdbc:oracle:thin:@127.0.0.1:1521:xe");
//	request.setConnectionParams(connectionParams);
//	request.setQuery("select sysdate from dual");
//	try {
//	    SQLExecResponse response = engine.handleRequest(request);
//	    for (String columnName : response.getColumnNames()) {
//		System.out.println(columnName);
//	    }
//	    for (SQLRow row : response.getSqlRows()) {
//
//		for (String columnValue : row.getColumnValues()) {
//		    System.out.println(columnValue);
//		}
//	    }
//	} catch (ExecutionException e) {
//	    // TODO Auto-generated catch block
//	    e.printStackTrace();
//	}
//    }
//
//    @Test
//    public void testUpdateQueryExecute() {
//	ITaskActionHandler<SQLExecRequest, SQLExecResponse> engine = new SQLTaskActionHandler();
//
//	SQLExecRequest request = new SQLExecRequest();
//	SQLConnectionParams connectionParams = new SQLConnectionParams();
//	connectionParams.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//	connectionParams.setUserName("selfcare");
//	connectionParams.setPassword("selfcare123");
//	connectionParams
//		.setConnectionString("jdbc:oracle:thin:@127.0.0.1:1521:xe");
//	request.setConnectionParams(connectionParams);
//	request.setQuery("insert into ITDB_ROLLING_MESSAGE values (7, 'test', 'green')");
//	try {
//	    SQLExecResponse response = engine.handleRequest(request);
//
//	    System.out.println(response.getnRowsAffected());
//	} catch (ExecutionException e) {
//	    // TODO Auto-generated catch block
//	    e.printStackTrace();
//	}
//    }
}
