/**
 * 
 */
package org.zcode.lexec.builder;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zcode.lexec.builder.CTaskBuilder;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.json.Strategy;
import org.zcode.lexec.json.SubTask;

/**
 * @author fkhan
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CTaskBuilderTest {

	@Autowired
	CTaskBuilder cTaskBuilder;
	
	 @Test
	 public void testCTaskBuild(){
		 
		 Strategy s = new Strategy();
		 s.setName("Test");
		 List<SubTask> subTaskList = new ArrayList<>();
		 SubTask subTask = new SubTask();
		 subTask.setType("SQL");
		 subTask.setSqlQuery("select * from abc");
		 subTaskList.add(subTask);
		 
		 subTask = new SubTask();
		 subTask.setType("CONTROL");
		 subTask.setExpression("IF #test == 0");
		 subTaskList.add(subTask);
		 
		 subTask = new SubTask();
		 subTask.setType("SQL");
		 subTask.setSqlQuery("select * from DEF");
		 subTaskList.add(subTask);

		 subTask = new SubTask();
		 subTask.setType("CONTROL");
		 subTask.setExpression("IF #test2 == 0");
		 subTaskList.add(subTask);

		 subTask = new SubTask();
		 subTask.setType("SQL");
		 subTask.setSqlQuery("select * from HIJ");
		 subTaskList.add(subTask);

		 subTask = new SubTask();
		 subTask.setType("CONTROL");
		 subTask.setExpression("ENDIF");
		 subTaskList.add(subTask);

		 subTask = new SubTask();
		 subTask.setType("SQL");
		 subTask.setSqlQuery("select * from JKL");
		 subTaskList.add(subTask);

		 subTask = new SubTask();
		 subTask.setType("CONTROL");
		 subTask.setExpression("ENDIF");
		 subTaskList.add(subTask);
		 
		 s.setTasks(subTaskList);
		 
		 Task t = cTaskBuilder.buildTask(s);
		 
		 System.out.println(t);
	 }
}
