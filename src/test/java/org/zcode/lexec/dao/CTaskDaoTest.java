package org.zcode.lexec.dao;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zcode.lexec.domain.CTask;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.domain.SQLTask;
import org.zcode.lexec.domain.Task;
import org.zcode.lexec.repo.CTaskRepo;
import org.zcode.lexec.sqlx.dao.SQLTaskDao;
import org.zcode.lexec.wsx.dao.SOAPTaskDao;

/**
 * @author Faisal_Khan01
 * Sep 23, 2016
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//Annotation to rollbak after save
//@Transactional
public class CTaskDaoTest {

    @Autowired
    private CTaskRepo cTaskRepo;
    
    @Autowired
    private SQLTaskDao sqlTaskDao;
    
    @Autowired
    private SOAPTaskDao wsTaskDao;
    
    @Test
    public void testSave(){
	CTask task = new CTask();
	task.setName("Test");
	task.setDescription("hi");
	List<Task> subTasks = new ArrayList<Task>();
	SQLTask sqlTask = sqlTaskDao.findOne("57f49c31ab08df1ec8adf456");
	sqlTask.setDescription("SQL Task 1");
	subTasks.add(sqlTask);
	SOAPTask wsTask = wsTaskDao.findOne("57f49c7eab08df19f001bad4");
	wsTask.setName("WS Task 1");
	subTasks.add(wsTask);
	task.setChildTasks(subTasks);
	
	cTaskRepo.save(task);
    }
    
    @Test
    public void testTaskRetrieval(){
	List<CTask> cTaskList = cTaskRepo.findByNameIgnoreCaseLikeOrDescriptionIgnoreCaseLike("Description", "Description");
	System.out.println("*****"+cTaskList.size());
    }
}
