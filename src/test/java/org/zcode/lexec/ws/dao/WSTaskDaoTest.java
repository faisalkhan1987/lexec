package org.zcode.lexec.ws.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.zcode.lexec.domain.SOAPTask;
import org.zcode.lexec.wsx.dao.SOAPTaskDao;

/**
 * @author Faisal_Khan01
 * Sep 22, 2016
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//Annotation to rollbak after save
//@Transactional
public class WSTaskDaoTest {

    @Autowired
    private SOAPTaskDao wsConfigDao;
    
    @Test
    public void testSave(){
	SOAPTask wsConfig = new SOAPTask();
	wsConfig.setName("TestWS");
	wsConfig.setWsEndpoint("http://www.google.com");
	wsConfig.setWsRequest("<a>123123</a>");
	wsConfigDao.save(wsConfig);
    }
		
}
